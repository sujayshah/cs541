#version 130


uniform vec3 aPos;

uniform mat4 light_space_matrix;

void main()
{
  
     vec4 pos  = light_space_matrix * vec4(aPos, 1.0);
     gl_Position = pos.xyww;
}