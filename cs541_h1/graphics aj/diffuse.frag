#version 130
uniform vec3 speclight_color;
uniform vec3 mainlight_color;
uniform vec3 diffuse_color;
uniform vec3 amb_color;
uniform int texShape;
in vec4 normal_vector;
in vec4 light_vector;
in vec4 view_vector;
in vec2 vtexture_coord;
out vec4 frag_color;
vec3 result_light;
uniform sampler2D usampler;
void main(void) 
{
	
	vec3 amb= amb_color;
	vec3 tcolor=texture(usampler,vtexture_coord).xyz;
	vec4 m = normalize(normal_vector);
	vec4 L = normalize(light_vector);
	vec4 view_dir=normalize(view_vector);
	vec4 h  = normalize(view_dir+L);
	vec4 reflect_dir=2*dot(m,L)*m-L;
	vec3 specular = vec3(0);
	
	if(dot(m, L) > 0) 
	{
	  float spec=pow(max(dot(m,h),0.0),100);
	  specular=spec*speclight_color*mainlight_color;
	}
		vec3 diffuse = max(dot(m, L), 0) * mainlight_color ;
		
	if(texShape!=1)
	{
		result_light=(diffuse+amb)* diffuse_color;
	}
	else
	{
		result_light=(diffuse+amb)* tcolor;
	}
	frag_color = vec4(result_light+specular, 1);
}

