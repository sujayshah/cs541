// cs541fbo-template.cpp
// -- template for assignment #4 (frame buffer objects)
// cs541 10/17
//
// To compile from Visual Studio 2015 command prompt:
//    cl /EHsc cs541fbo-template.cpp Affine3D.cpp Plane.cpp
//       opengl32.lib glew32.lib sdl2.lib sdl2main.lib
//       /link /subsystem:console
// To compile from Linux command line:
//    g++ cs541fbo-template.cpp Affine3D.cpp Plane.cpp
//        -lGL -lGLEW -lSDL2

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <sstream>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include "Affine3D.h"
#include "Plane.h"
using namespace std;


const int MESH_SIZE = 20;
const float PI = 4.0f*atan(1.0f);

const int FRAME_BUFFER_W = 1024,
          FRAME_BUFFER_H = 1024;


// load a text file into a string
string loadTextFile(const char *fname) {
  string out,
         line;
  ifstream in(fname);
  getline(in,line);
  while (in) {
    out += line + "\n";
    getline(in,line);
  }
  return out;
}


// read a 24-bit color bitmap image file
//   Note: caller is responsible for deallocating the
//         RGB data
unsigned char* loadBitmapFile(const char *fname, int *width, int *height) {
  fstream in(fname,ios_base::in|ios_base::binary);
  char header[38];
  in.read(header,38);
  unsigned offset = *reinterpret_cast<unsigned*>(header+10);
  int W = *reinterpret_cast<int*>(header+18),
      H = *reinterpret_cast<int*>(header+22),
      S = 3*W;
  S += (4-S%4)%4;
  int size = S*H;
  in.seekg(offset,ios_base::beg);
  unsigned char *data = new unsigned char[size];
  in.read(reinterpret_cast<char*>(data),size);
  if (!in) {
    delete[] data;
    return 0;
  }

  for (int j=0; j < H; ++j) {
    for (int i=0; i < W; ++i) {
      int index = S*j + 3*i;
      swap(data[index+0],data[index+2]);
    }
  }

  *width = W;
  *height = H;
  return data;
}


/////////////////////////////////////////////////////////////////
class Client {
  public:
    Client(void);
    ~Client(void);
    void draw(double dt);
    void keypress(SDL_Keycode kc);
    void resize(int W, int H);
    void mousedrag(int x, int y, bool lbutton);
  private:
    GLuint vertex_buffer,
           normal_buffer,
           face_buffer,
           texcoord_buffer,
           texture_buffer,
           frame_buffer,
           depth_buffer,
           frame_buffer_texture_buffer;
    GLint program,
          aposition,
          anormal,
          atexture_coord,
          upersp_matrix,
          uview_matrix,
          umodel_matrix,
          unormal_matrix,
          ulight_position,
          ulight_color;
    int face_count;
    float time;
    float aspect;
};


Client::Client(void) {
  // (I) create shader program
  GLint value;

  // (I.A) compile fragment shader
  string fragtextstr = loadTextFile("diffuse_texture.frag");
  const char *fragtext = fragtextstr.c_str();
  GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fshader,1,&fragtext,0);
  glCompileShader(fshader);
  glGetShaderiv(fshader,GL_COMPILE_STATUS,&value);
  if (!value) {
    cerr << "fragment shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(fshader,1024,0,buffer);
    cerr << buffer << endl;
  }

  // (I.B) compile vertex shader
  string verttextstr = loadTextFile("diffuse_texture.vert").c_str();
  const char *verttext = verttextstr.c_str();
  GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vshader,1,&verttext,0);
  glCompileShader(vshader);
  glGetShaderiv(vshader,GL_COMPILE_STATUS,&value);
  if (!value) {
    cerr << "vertex shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(vshader,1024,0,buffer);
    cerr << buffer << endl;
  }

  // (I.C) link shaders
  program = glCreateProgram();
  glAttachShader(program,fshader);
  glAttachShader(program,vshader);
  glLinkProgram(program);
  glGetProgramiv(program,GL_LINK_STATUS,&value);
  if (!value) {
    cerr << "vertex shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(vshader,1024,0,buffer);
    cerr << buffer << endl;
  }
  glDeleteShader(fshader);
  glDeleteShader(vshader);

  // (II) get shader variable locations
  aposition = glGetAttribLocation(program,"position");
  anormal = glGetAttribLocation(program,"normal");
  atexture_coord = glGetAttribLocation(program,"texture_coord");
  upersp_matrix = glGetUniformLocation(program,"persp_matrix");
  uview_matrix = glGetUniformLocation(program,"view_matrix");
  umodel_matrix = glGetUniformLocation(program,"model_matrix");
  unormal_matrix = glGetUniformLocation(program,"normal_matrix");
  ulight_position = glGetUniformLocation(program,"light_position");
  ulight_color = glGetUniformLocation(program,"light_color");

  // (III) upload plane mesh to GPU
  Plane plane(MESH_SIZE);
  face_count = plane.faceCount();

  // (III.A) vertex buffer
  glGenBuffers(1,&vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer);
  int vertex_buffer_size = sizeof(Hcoord)*plane.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,plane.vertexArray(),GL_STATIC_DRAW);

  // (III.B) normal buffer
  glGenBuffers(1,&normal_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,plane.normalArray(),GL_STATIC_DRAW);

  // (III.C) face buffer
  glGenBuffers(1,&face_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,plane.faceArray(),GL_STATIC_DRAW);

  // (III.D) texture coordinate buffer
  glGenBuffers(1,&texcoord_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);
  float *texcoord = new float[2*plane.vertexCount()];
  Matrix Std2Unit = scale(0.5f,0.5f,1)
                    * translate(Hcoord(1,1,0,0));
  for (int i=0; i < plane.vertexCount(); ++i) {
    Hcoord uv = Std2Unit * plane.vertexArray()[i];
    texcoord[2*i+0] = uv[0];
    texcoord[2*i+1] = uv[1];
  }
  glBufferData(GL_ARRAY_BUFFER,2*sizeof(float)*plane.vertexCount(),texcoord,GL_STATIC_DRAW);
  delete[] texcoord;

  // (IV) upload texture to GPU
  int width, height;
  unsigned char *rgbdata = loadBitmapFile("image.bmp",&width,&height);
  glGenTextures(1,&texture_buffer);
  glBindTexture(GL_TEXTURE_2D,texture_buffer);
  glPixelStorei(GL_UNPACK_ALIGNMENT,4);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,rgbdata);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

  // (V) enable use of z-buffer
  glEnable(GL_DEPTH_TEST);


  // create a frame buffer object to use a texture
  // (1) frame buffer
  glGenFramebuffers(1,&frame_buffer);
  glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer);
  // (2) texture associated to frame buffer
  glGenTextures(1,&frame_buffer_texture_buffer);
  glBindTexture(GL_TEXTURE_2D,frame_buffer_texture_buffer);
  glPixelStorei(GL_UNPACK_ALIGNMENT,4);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,FRAME_BUFFER_W,FRAME_BUFFER_H,
               0,GL_RGB,GL_UNSIGNED_BYTE,0);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  // (3) depth buffer
  glGenRenderbuffers(1,&depth_buffer);
  glBindRenderbuffer(GL_RENDERBUFFER,depth_buffer);
  glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,1024,1024);
  // (4) attach depth buffer to frame buffer
  glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,
                            GL_RENDERBUFFER,depth_buffer);
  // (5) attach texture to frame buffer
  glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,
                         GL_TEXTURE_2D,frame_buffer_texture_buffer,0);
  // (6) make sure everything is ok
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    cout << "frame buffer failure" << endl;

  aspect = 1;
  time = 0;
}


Client::~Client(void) {
  // delete the shader program
  glUseProgram(0);
  glDeleteProgram(program);

  // delete the vertex, normal, texture coordinate, and face buffers
  glDeleteBuffers(1,&face_buffer);
  glDeleteBuffers(1,&normal_buffer);
  glDeleteBuffers(1,&vertex_buffer);
  glDeleteBuffers(1,&texcoord_buffer);

  // delete texture buffer
  glDeleteTextures(1,&texture_buffer);

  // delete frame buffer
  glDeleteRenderbuffers(1,&depth_buffer);
  glDeleteTextures(1,&frame_buffer_texture_buffer);
  glDeleteFramebuffers(1,&frame_buffer);
}


void Client::draw(double dt) {
  // shader values and attributes used by both rendering passes
  glUseProgram(program);
  Matrix P = perspective(80,aspect,0.1f);
  glUniformMatrix4fv(upersp_matrix,1,true,(float*)&P);
  Matrix V = scale(1);
  glUniformMatrix4fv(uview_matrix,1,true,(float*)&V);
  const Hcoord LIGHT_POSITION(1,0.5f,0,1),
               LIGHT_COLOR(1,1,1,1);
  glUniform4fv(ulight_position,1,(float*)&LIGHT_POSITION);
  glUniform3fv(ulight_color,1,(float*)&LIGHT_COLOR);

  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);

  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);
  glVertexAttribPointer(atexture_coord,2,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(atexture_coord);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer);

  ///////////////////////////////////////////////////////////////
  // pass #1: render to frame buffer (use texture from file)
  ///////////////////////////////////////////////////////////////
  glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer);
  int viewport_save[4];
  glGetIntegerv(GL_VIEWPORT,viewport_save);
  glViewport(0,0,FRAME_BUFFER_W,FRAME_BUFFER_H);

  glClearColor(1,0.8f,0.8f,1);
  glClearDepth(1);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // draw using texture from file
  const float RATE = 360.0f/5.0f;
  const Hcoord AXIS(0,1,0,0),
               CENTER(0,0,-3,0);
  Matrix M = translate(CENTER)
             * rotate(RATE*time,AXIS)
             * scale(1.5f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);
  glBindTexture(GL_TEXTURE_2D,texture_buffer);
  glDrawElements(GL_TRIANGLES,3*face_count,GL_UNSIGNED_INT,0);

  ///////////////////////////////////////////////////////////////
  // pass #2: render to screen (use both texture from file
  //                            and frame buffer texture)
  ///////////////////////////////////////////////////////////////
  glBindFramebuffer(GL_FRAMEBUFFER,0);
  glViewport(viewport_save[0],viewport_save[1],viewport_save[2],viewport_save[3]);

  glClearColor(0.9f,0.9f,0.9f,1);
  glClearDepth(1);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // draw using texture from file
  glBindTexture(GL_TEXTURE_2D,texture_buffer);
  glDrawElements(GL_TRIANGLES,3*face_count,GL_UNSIGNED_INT,0);

  // draw using frame buffer texture
  const Hcoord EZ(0,0,1,0);
  M = translate(Hcoord(0.75f,-0.75f,-2,0))
      * rotate(-0.5f*RATE*time,EZ)
      * scale(0.5f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);
  glBindTexture(GL_TEXTURE_2D,frame_buffer_texture_buffer);
  glDrawElements(GL_TRIANGLES,3*face_count,GL_UNSIGNED_INT,0);

  time += dt;
}


void Client::keypress(SDL_Keycode kc) {
  // respond to keyboard input
  //   kc: SDL keycode (e.g., SDLK_SPACE, SDLK_a, SDLK_s)
}


void Client::resize(int W, int H) {
  aspect = float(W)/float(H);
  glViewport(0,0,W,H);
}


void Client::mousedrag(int x, int y, bool left_button) {
  // respond to mouse click
  //   (x,y): click location (in window coordinates)
}


/////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {

  // SDL: initialize and create a window
  SDL_Init(SDL_INIT_VIDEO);
  const char *title = "Window Title";
  int width = 500,
      height = 500;
  SDL_Window *window = SDL_CreateWindow(title,SDL_WINDOWPOS_UNDEFINED,
                                        SDL_WINDOWPOS_UNDEFINED,width,height,
                                        SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
  SDL_GLContext context = SDL_GL_CreateContext(window);

  // GLEW: get function bindings (if possible)
  glewInit();
  if (!GLEW_VERSION_2_0) {
    cout << "needs OpenGL version 2.0 or better" << endl;
    return -1;
  }

  // animation loop
  bool done = false;
  Client *client = new Client();
  Uint32 ticks_last = SDL_GetTicks();
  while (!done) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          done = true;
          break;
        case SDL_KEYDOWN:
          if (event.key.keysym.sym == SDLK_ESCAPE)
            done = true;
          else
            client->keypress(event.key.keysym.sym);
          break;
        case SDL_WINDOWEVENT:
          if (event.window.event == SDL_WINDOWEVENT_RESIZED)
            client->resize(event.window.data1,event.window.data2);
          break;
        case SDL_MOUSEMOTION:
          if ((event.motion.state&SDL_BUTTON_LMASK) != 0
              || (event.motion.state&SDL_BUTTON_RMASK) != 0)
            client->mousedrag(event.motion.xrel,event.motion.yrel,
                              (event.motion.state&SDL_BUTTON_LMASK) != 0);
          break;
      }
    }
    Uint32 ticks = SDL_GetTicks();
    double dt = 0.001*(ticks - ticks_last);
    ticks_last = ticks;
    client->draw(dt);
    SDL_GL_SwapWindow(window);
  }

  // clean up
  delete client;
  SDL_GL_DeleteContext(context);
  SDL_Quit();
  return 0;
}

