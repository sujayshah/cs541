// cs541transform-template.cpp
// -- template for assignment #1 (modeling transforms)
// cs541 9/17
//  Sujay Shah 60001517
// To compile from Visual Studio 2015 command prompt:
//    cl /EHsc cs541transform-template.cpp Affine3D.cpp Torus.cpp Plane.cpp Sphere.cpp opengl32.lib glew32.lib sdl2.lib sdl2main.lib /link /subsystem:console


#include <iostream>
#include <algorithm>
#include "SDL2/SDL.h"
#include "GL/glew.h"
#include "GL/gl.h"
#include "Affine3D.h"
#include "Torus.h"
#include "Sphere.h"
#include "Plane.h"
#include <fstream>
#include <sstream>
using namespace std;

static double dt;
const int MESH_SIZE = 20;
const float THICKNESS = 0.5f;
const float PI = 4.0f*atan(1.0f);
const float RATE = 360.0f/5.0f;
  const Hcoord AXIS(0,1,0,0),
               CENTER(0.0f,0.0f,-3.0f,0);
Hcoord pos_torus(-2.0f,2.0f,-3.0f,0),pos_sphere(0,0,-3,0),pos_plane(0,0,-3,0),pos_sktorus(0,0,-3,0),pos_Tplane(0,0,-7,0);
Hcoord O(0,0,0,0), EYAXIS(0,1,0,0),look,eye, u,light_position;
Matrix P;

const int FRAME_BUFFER_W = 1024,
          FRAME_BUFFER_H = 1024;
		  
int fov=80,light_switch=0;
class Client {
  public:
    Client(void);
    ~Client(void);
    void draw(double dt);
	void draw_Torus();
	void draw_Sphere();
	void draw_Plane();
	void draw_Tplane();
	void draw_sktorus();
	void initialise_Torus();
	void initialise_Sphere();
	void initialise_Plane();
	void initialise_Tplane();
	void initialise_skTorus();
    void keypress(SDL_Keycode kc);
    void resize(int W, int H);
    void mousedrag(int x, int y, bool lbutton);
  private:
    GLuint vertex_buffer_torus,
           normal_buffer_torus,
           face_buffer_torus,
		   vertex_buffer_sphere,
           normal_buffer_sphere,
           face_buffer_sphere,
		   vertex_buffer_plane,
           normal_buffer_plane,
		   face_buffer_plane,
		   vertex_buffer_Tplane,
           normal_buffer_Tplane,
		   face_buffer_Tplane,
		   texcoord_buffer,
		   texture_buffer,
		   vertex_buffer_sktorus,
           normal_buffer_sktorus,
           face_buffer_sktorus,
		   frame_buffer,
		   frame_buffer_texture_buffer,
		   depth_buffer;
    GLint program,
          aposition,
          anormal,
		  atexture_coord,
          upersp_matrix,
          uview_matrix,
          umodel_matrix,
          unormal_matrix,
          //udiffuse_color;
	      udiffuse_color,
		  ulight_color,
		  uspec_color,
		  uamb_color,
		  ucamera_pos,
		  ulight_position,
		  texShape;
    int face_count_torus,face_count_sphere,face_count_plane,face_count_sktorus,face_count_Tplane;
	Matrix sktorus_model;
    float time;
    float aspect;
	Hcoord mainLightColor;
};


// load a text file into a string
string loadTextFile(const char *fname) {
  string out,
         line;
  ifstream in(fname);
  getline(in,line);
  while (in) {
    out += line + "\n";
    getline(in,line);
  }
  return out;
}

// read a 24-bit color bitmap image file
//   Note: caller is responsible for deallocating the
//         RGB data
unsigned char* loadBitmapFile(const char *fname, int *width, int *height) {
  fstream in(fname,ios_base::in|ios_base::binary);
  char header[38];
  in.read(header,38);
  unsigned offset = *reinterpret_cast<unsigned*>(header+10);
  int W = *reinterpret_cast<int*>(header+18),
      H = *reinterpret_cast<int*>(header+22),
      S = 3*W;
  S += (4-S%4)%4;
  int size = S*H;
  in.seekg(offset,ios_base::beg);
  unsigned char *data = new unsigned char[size];
  in.read(reinterpret_cast<char*>(data),size);
  if (!in) {
    delete[] data;
    return 0;
  }

  for (int j=0; j < H; ++j)
   {
    for (int i=0; i < W; ++i)
     {
      int index = S*j + 3*i;
      swap(data[index+0],data[index+2]);
    }
  }
  *width = W;
  *height = H;
  return data;
}

Client::Client(void) {
 
  GLint value;
  
// (I.A) compile fragment shader
  string fragtextstr = loadTextFile("diffuse.frag");
  const char *fragtext = fragtextstr.c_str();
  GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fshader,1,&fragtext,0);
  glCompileShader(fshader);
  glGetShaderiv(fshader,GL_COMPILE_STATUS,&value);
  if (!value) {
    cerr << "fragment shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(fshader,1024,0,buffer);
    cerr << buffer << endl;
  }

  // (I.B) compile vertex shader
  string verttextstr = loadTextFile("diffuse.vert").c_str();
  const char *verttext = verttextstr.c_str();
  GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vshader,1,&verttext,0);
  glCompileShader(vshader);
  glGetShaderiv(vshader,GL_COMPILE_STATUS,&value);
  if (!value) {
    cerr << "vertex shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(vshader,1024,0,buffer);
    cerr << buffer << endl;
  }

  // (I.C) link shaders
  program = glCreateProgram();
  glAttachShader(program,fshader);
  glAttachShader(program,vshader);
  glLinkProgram(program);
  glGetProgramiv(program,GL_LINK_STATUS,&value);
  if (!value) {
    cerr << "vertex shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(vshader,1024,0,buffer);
    cerr << buffer << endl;
  }
  glDeleteShader(fshader);
  glDeleteShader(vshader);
  
  aposition = glGetAttribLocation(program,"position");
  anormal = glGetAttribLocation(program,"normal");
  upersp_matrix = glGetUniformLocation(program,"persp_matrix");
  uview_matrix = glGetUniformLocation(program,"view_matrix");
  umodel_matrix = glGetUniformLocation(program,"model_matrix");
  unormal_matrix = glGetUniformLocation(program,"normal_matrix");
  //udiffuse_color = glGetUniformLocation(program,"color");
  udiffuse_color= glGetUniformLocation(program, "diffuse_color");
  uspec_color = glGetUniformLocation(program, "speclight_color");
  ulight_color = glGetUniformLocation(program, "mainlight_color");
  uamb_color=glGetUniformLocation(program, "amb_color");
  ulight_position= glGetUniformLocation(program, "light_position");
  ucamera_pos= glGetUniformLocation(program, "camera_pos");
  atexture_coord = glGetAttribLocation(program,"texture_coord");
  texShape = glGetUniformLocation(program,"texShape");
  
  initialise_Torus();
  initialise_Sphere();
  initialise_Plane();
  initialise_skTorus();
  initialise_Tplane();
  eye=Hcoord (0,0,0,1);
  look= Hcoord(0,0,-1,0);
 
  light_position.z = -5;
 
  mainLightColor.x = 1;
  mainLightColor.y = 1;
  mainLightColor.z = 1;
	
	// create a frame buffer object to use a texture
  // (1) frame buffer
  glGenFramebuffers(1,&frame_buffer);
  glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer);
  // (2) texture associated to frame buffer
  glGenTextures(1,&frame_buffer_texture_buffer);
  glBindTexture(GL_TEXTURE_2D,frame_buffer_texture_buffer);
  glPixelStorei(GL_UNPACK_ALIGNMENT,4);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,FRAME_BUFFER_W,FRAME_BUFFER_H,
               0,GL_RGB,GL_UNSIGNED_BYTE,0);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  // (3) depth buffer
  glGenRenderbuffers(1,&depth_buffer);
  glBindRenderbuffer(GL_RENDERBUFFER,depth_buffer);
  glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,1024,1024);
  // (4) attach depth buffer to frame buffer
  glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,
                            GL_RENDERBUFFER,depth_buffer);
  // (5) attach texture to frame buffer
  glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,
                         GL_TEXTURE_2D,frame_buffer_texture_buffer,0);
  // (6) make sure everything is ok
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    cout << "frame buffer failure" << endl;

  glUseProgram(program);
	// have to know which program var to init
  glUniform3f(ulight_color, mainLightColor.x, mainLightColor.y, mainLightColor.z);
  glUniform3f(uspec_color, 1, 1, 1);
  glUniform3f(uamb_color, 0.3f, 0.3f, 0.3f);
  
   aspect = 1;
  time = 0;
  //=0;
}
void Client:: initialise_Torus()
{
	
	//  uploading torus mesh to GPU
  Torus torus(THICKNESS,MESH_SIZE);
  face_count_torus = torus.faceCount();

  // torus vertex buffer
  glGenBuffers(1,&vertex_buffer_torus);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_torus);
  int vertex_buffer_size = sizeof(Hcoord)*torus.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,torus.vertexArray(),GL_STATIC_DRAW);

  // torus normal buffer
  glGenBuffers(1,&normal_buffer_torus);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_torus);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,torus.normalArray(),GL_STATIC_DRAW);

  // torus face buffer
  glGenBuffers(1,&face_buffer_torus);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_torus);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count_torus;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,torus.faceArray(),GL_STATIC_DRAW);

  //  enable use of z-buffer
  glEnable(GL_DEPTH_TEST);
}

void Client:: initialise_Sphere()
{
	//  upload sphere mesh to GPU
  Sphere sphere(MESH_SIZE);
  face_count_sphere = sphere.faceCount();

  //  vertex buffer of sphere
  glGenBuffers(1,&vertex_buffer_sphere);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_sphere);
  int vertex_buffer_size = sizeof(Hcoord)*sphere.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,sphere.vertexArray(),GL_STATIC_DRAW);

  //  normal buffer of sphere
  glGenBuffers(1,&normal_buffer_sphere);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_sphere);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,sphere.normalArray(),GL_STATIC_DRAW);

  //  face buffer of sphere
  glGenBuffers(1,&face_buffer_sphere);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_sphere);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count_sphere;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,sphere.faceArray(),GL_STATIC_DRAW);

  // (IV) enable use of z-buffer
  glEnable(GL_DEPTH_TEST);
}
void Client:: initialise_Plane()
{
	//  upload plane mesh to GPU
  Plane plane(MESH_SIZE);
  face_count_plane = plane.faceCount();

  //  vertex buffer of plane
  glGenBuffers(1,&vertex_buffer_plane);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_plane);
  int vertex_buffer_size = sizeof(Hcoord)*plane.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,plane.vertexArray(),GL_STATIC_DRAW);

  //  normal buffer of plane
  glGenBuffers(1,&normal_buffer_plane);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_plane);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,plane.normalArray(),GL_STATIC_DRAW);

  //  face buffer of plane
  glGenBuffers(1,&face_buffer_plane);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_plane);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count_plane;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,plane.faceArray(),GL_STATIC_DRAW);

  //  enable use of z-buffer
  glEnable(GL_DEPTH_TEST);
}


void Client::initialise_Tplane()
{
  Plane tplane(MESH_SIZE);
  face_count_Tplane = tplane.faceCount();

  // (III.A) vertex buffer
  glGenBuffers(1,&vertex_buffer_Tplane);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_Tplane);
  int vertex_buffer_size = sizeof(Hcoord)*tplane.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,tplane.vertexArray(),GL_STATIC_DRAW);

  // (III.B) normal buffer
  glGenBuffers(1,&normal_buffer_Tplane);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_Tplane);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,tplane.normalArray(),GL_STATIC_DRAW);

  // (III.C) face buffer
  glGenBuffers(1,&face_buffer_Tplane);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_Tplane);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count_Tplane;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,tplane.faceArray(),GL_STATIC_DRAW);

  // (III.D) texture coordinate buffer
  glGenBuffers(1,&texcoord_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);
  float *texcoord = new float[2*tplane.vertexCount()];
  Matrix Std2Unit = scale(0.5f,0.5f,1)
                    * translate(Hcoord(1,1,0,0));
  for (int i=0; i < tplane.vertexCount(); ++i)
   {
    Hcoord uv = Std2Unit * tplane.vertexArray()[i];
    texcoord[2*i+0] = uv[0];
    texcoord[2*i+1] = uv[1];
  }
  glBufferData(GL_ARRAY_BUFFER,2*sizeof(float)*tplane.vertexCount(),texcoord,GL_STATIC_DRAW);
  delete[] texcoord;

  // (IV) upload texture to GPU
  int width, height;
  unsigned char *rgbdata = loadBitmapFile("image.bmp",&width,&height);
  glGenTextures(1,&texture_buffer);
  glBindTexture(GL_TEXTURE_2D,texture_buffer);
  glPixelStorei(GL_UNPACK_ALIGNMENT,4);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,rgbdata);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
  
  
}
void Client:: initialise_skTorus()
{
	//  upload torus mesh to GPU
  Torus sktorus(THICKNESS,MESH_SIZE);
  face_count_sktorus = sktorus.faceCount();

  //  vertex buffer of sktorus
  glGenBuffers(1,&vertex_buffer_sktorus);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_sktorus);
  int vertex_buffer_size = sizeof(Hcoord)*sktorus.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,sktorus.vertexArray(),GL_STATIC_DRAW);

  //  normal buffer of sktorus
  glGenBuffers(1,&normal_buffer_sktorus);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_sktorus);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,sktorus.normalArray(),GL_STATIC_DRAW);

  //  face buffer of sktorus
  glGenBuffers(1,&face_buffer_sktorus);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_sktorus);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count_sktorus;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,sktorus.faceArray(),GL_STATIC_DRAW);
	
	//initialise model
	sktorus_model = translate(pos_sktorus) * scale(1.0f,1.0f,0.5f);
	
  //  enable use of z-buffer
  glEnable(GL_DEPTH_TEST);
}
Client::~Client(void) {
  // delete the shader program
  glUseProgram(0);
  GLuint shaders[2];
  GLsizei count;
  glGetAttachedShaders(program,2,&count,shaders);
  for (int i=0; i < count; ++i)
    glDeleteShader(shaders[i]);
  glDeleteProgram(program);

  // delete the vertex, normal, and face buffers
  glDeleteBuffers(1,&face_buffer_torus);
  glDeleteBuffers(1,&normal_buffer_torus);
  glDeleteBuffers(1,&vertex_buffer_torus);
  glDeleteBuffers(1,&face_buffer_sphere);
  glDeleteBuffers(1,&normal_buffer_sphere);
  glDeleteBuffers(1,&vertex_buffer_sphere);
  glDeleteBuffers(1,&face_buffer_plane);
  glDeleteBuffers(1,&normal_buffer_plane);
  glDeleteBuffers(1,&vertex_buffer_plane);
  glDeleteBuffers(1,&face_buffer_sktorus);
  glDeleteBuffers(1,&normal_buffer_sktorus);
  glDeleteBuffers(1,&vertex_buffer_sktorus);
  glDeleteBuffers(1,&face_buffer_Tplane);
  glDeleteBuffers(1,&normal_buffer_Tplane);
  glDeleteBuffers(1,&vertex_buffer_Tplane);
}



void Client::draw_Torus()
{
	Hcoord axis_y(0,1,0,0);
	
	Matrix R = rotate(RATE*time,axis_y),
         M = translate(pos_torus)
             * R
             * scale(0.2f*sin(time));
	
	
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&R);
  glUniform3f(udiffuse_color,abs(cosf(time)),0.8,0.5);
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_torus);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_torus);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  // draw the mesh torus
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_torus);
  glDrawElements(GL_TRIANGLES,3*face_count_torus,GL_UNSIGNED_INT,0);
}
void Client::draw_Sphere()
{
	
	Matrix R = rotate(RATE*time,AXIS),
         M = translate(pos_sphere)
             * R
             * scale(0.2f);
			 
	
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&R);
  glUniform3f(udiffuse_color,0.8,0,abs(cosf(time)*1));
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_sphere);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_sphere);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  // draw the mesh sphere
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_sphere);
  glDrawElements(GL_TRIANGLES,3*face_count_sphere,GL_UNSIGNED_INT,0);
}
void Client::draw_Plane()
{
	Matrix R = rotate(RATE*time,AXIS),
         M = translate(pos_plane)
             * R
             * scale(.2f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&R);
  glUniform3f(udiffuse_color,0.5,1,0);
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_plane);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_plane);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  // draw the mesh plane
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_plane);
  glDrawElements(GL_TRIANGLES,3*face_count_plane,GL_UNSIGNED_INT,0);
}

void Client::draw_sktorus()
{
	// Normal matrix

			 
	Matrix U = translate(pos_sktorus) * rotate(RATE*time,AXIS) * translate(-pos_sktorus);
	Matrix X = U*sktorus_model;
	Matrix N = transpose3x3(inverse3x3(X));
	
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&X);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&N);
  glUniform3f(udiffuse_color,0.8,0,1);
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_sktorus);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_sktorus);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  // draw the mesh sphere
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_sktorus);
  glDrawElements(GL_TRIANGLES,3*face_count_sktorus,GL_UNSIGNED_INT,0);
}


void Client::draw_Tplane()
{
	//printf("in draw tplane\n");
	 Matrix M = translate(pos_Tplane)
             * rotate(RATE*time,AXIS)
             * scale(1.5f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);
  
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_Tplane);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);

  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_Tplane);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);
  glVertexAttribPointer(atexture_coord,2,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(atexture_coord);

  // select the texture to use
  glBindTexture(GL_TEXTURE_2D,texture_buffer);

  // draw the mesh
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_Tplane);
  glDrawElements(GL_TRIANGLES,3*face_count_Tplane,GL_UNSIGNED_INT,0);
}
void Client::draw(double dt) 
{
	 ///////////////////////////////////////////////////////////////
  // pass #1: render to frame buffer (use texture from file)
  ///////////////////////////////////////////////////////////////
  glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer);
  int viewport_save[4];
  glGetIntegerv(GL_VIEWPORT,viewport_save);
  glViewport(0,0,FRAME_BUFFER_W,FRAME_BUFFER_H);

  glClearColor(1,0.8f,0.8f,1);
  glClearDepth(1);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  // clear frame buffer and z-buffer
	//white background
 /* glClearColor(0.8f,0.8f,0.8f,1);
  glClear(GL_COLOR_BUFFER_BIT);
  glClearDepth(1);
  glClear(GL_DEPTH_BUFFER_BIT);*/


  
  glUseProgram(program);
  glUniform1i(texShape , 0);
  //glUniform3f(ulight_color,1,1,1);

  glUniform4f(ulight_position, light_position.x, light_position.y, light_position.z,1);
  Matrix P = perspective(fov,aspect,0.1f);
  glUniformMatrix4fv(upersp_matrix,1,true,(float*)&P);
  Matrix V = scale(1);
 
  V.rows[0]=normalize(cross(look,EYAXIS));
  V.rows[2]=-normalize(look);
  V.rows[1]=cross(V.rows[2],V.rows[0]);
  V=V*translate(O-eye);
  glUniformMatrix4fv(uview_matrix,1,true,(float*)&V);

  Matrix R = rotate(0*time, AXIS),
	  M = translate(light_position)
	  * R
	  * scale(0.05f);
  //set light object
  glUniformMatrix4fv(umodel_matrix, 1, true, (float*)&M);
  glUniformMatrix4fv(unormal_matrix, 1, true, (float*)&R);
  glUniform3f(udiffuse_color, 0.0f, 0.0f, 0.0f);
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_sphere);
  glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);
  glEnableVertexAttribArray(aposition);
  glBindBuffer(GL_ARRAY_BUFFER, normal_buffer_sphere);
  glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);
  glEnableVertexAttribArray(anormal);

  // draw the mesh sphere
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, face_buffer_sphere);
  glDrawElements(GL_TRIANGLES, 3 * face_count_sphere, GL_UNSIGNED_INT, 0);

  //copy camera pos vals
  glUniform4f(ucamera_pos, eye.x, eye.y, eye.z, 1);
	
	for(int i = 0 ; i <10; i++)
	{
		pos_torus.x=-2.0f+0.4*i;
		pos_torus.y=2.0f;  
		draw_Torus();
		pos_sphere.x=-2.0f+0.4*i;
		pos_sphere.y=1.0f; 
		draw_Sphere();
		pos_plane.x=-2.0f+0.4*i;
		pos_plane.y=0.0f;  
		draw_Plane();		
  
		pos_torus.x=-2.0f+0.4*i;
		pos_torus.y=-1.0f;  
		draw_Torus();
		pos_sphere.x=-2.0f+0.4*i;
		pos_sphere.y=-1.5f; 
		draw_Sphere();
		pos_plane.x=-2.0f+0.4*i;
		pos_plane.y=-2.0f;  
		draw_Plane();
		
	}
	draw_sktorus();
	glUniform1i(texShape , 1);
	draw_Tplane();
	
glBindFramebuffer(GL_FRAMEBUFFER,0);

//glUniform1i(texShape , 1);
  // draw using texture from file
  /*const float RATE = 360.0f/5.0f;
  const Hcoord AXIS(0,1,0,0),
               CENTER(0,0,-3,0);
  Matrix M1 = translate(CENTER)
             * rotate(RATE*time,AXIS)
             * scale(1.5f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M1);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M1);
  glBindTexture(GL_TEXTURE_2D,texture_buffer);
  glDrawElements(GL_TRIANGLES,3*face_count_Tplane,GL_UNSIGNED_INT,0);*/

  ///////////////////////////////////////////////////////////////
  // pass #2: render to screen (use both texture from file
  //                            and frame buffer texture)
  ///////////////////////////////////////////////////////////////
  
  
  
 
    
  glViewport(viewport_save[0],viewport_save[1],viewport_save[2],viewport_save[3]);

  glClearColor(0.9f,0.9f,0.9f,1);
  glClearDepth(1);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // draw using texture from file
  //glBindTexture(GL_TEXTURE_2D,texture_buffer);
  //glDrawElements(GL_TRIANGLES,3*face_count_Tplane,GL_UNSIGNED_INT,0);
  
  draw_Tplane();
  
  
  
  
  
  
  
  
  
  
glUniform1i(texShape , 1);
  // draw using frame buffer texture
  const Hcoord EZ(0,0,1,0);
  M = translate(Hcoord(0.75f,-0.75f,-2,0))
      * rotate(-0.5f*RATE*time,EZ)
      * scale(0.5f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);
  glBindTexture(GL_TEXTURE_2D,frame_buffer_texture_buffer);
  glDrawElements(GL_TRIANGLES,3*face_count_Tplane,GL_UNSIGNED_INT,0);
  glBindTexture(GL_TEXTURE_2D,0);
glUniform1i(texShape , 0);
  //from file to screen
  for(int i = 0 ; i <10; i++)
	{
		pos_torus.x=-2.0f+0.4*i;
		pos_torus.y=2.0f;  
		draw_Torus();
		pos_sphere.x=-2.0f+0.4*i;
		pos_sphere.y=1.0f; 
		draw_Sphere();
		pos_plane.x=-2.0f+0.4*i;
		pos_plane.y=0.0f;  
		draw_Plane();		
  
		pos_torus.x=-2.0f+0.4*i;
		pos_torus.y=-1.0f;  
		draw_Torus();
		pos_sphere.x=-2.0f+0.4*i;
		pos_sphere.y=-1.5f; 
		draw_Sphere();
		pos_plane.x=-2.0f+0.4*i;
		pos_plane.y=-2.0f;  
		draw_Plane();
		
	}
	draw_sktorus();
	
  time += dt;
}


void Client::keypress(SDL_Keycode kc) 
{
	const float XLAT=0.1f,source_speed=60.0f;
  switch(kc)
  {
		case SDLK_RIGHT:
		  u=normalize(cross(look,EYAXIS));
		  eye=eye+XLAT*u;
		  break;
		  
		case SDLK_LEFT:
			u=normalize(cross(look,EYAXIS));
		    eye=eye-XLAT*u;
			break;
		
		case SDLK_UP:
			eye=eye+XLAT*look;
			break;
			
		case SDLK_DOWN:
			eye=eye-XLAT*look;
			break;
			
		case SDLK_w:
			if(fov>120)
				break;
			else
			fov+=20;
			break;
			
		case SDLK_s:
			if(fov<30)
				break;
			else;
			fov-=20;
			break;

		//light movement
		case SDLK_KP_5: //forward z
			light_position.z -= source_speed*dt;
			break;
		case SDLK_KP_2:// backward z
			light_position.z += source_speed*dt;
			break;
		case SDLK_KP_1://left x
			light_position.x -= source_speed*dt;
			break;
		case SDLK_KP_3://right x
			light_position.x += source_speed*dt;
			break;
		case SDLK_KP_7://up y
			light_position.y += source_speed*dt;
			break;
		case SDLK_KP_4://down y
			light_position.y -= source_speed*dt;
			break;
		case SDLK_l: //switch lights on/off
			if(light_switch%2==0)
			glUniform3f(ulight_color, 0, 0, 0);
			else
			{
				glUniform3f(ulight_color, mainLightColor.x, mainLightColor.y, mainLightColor.z);
			}
			light_switch++;
			break;

  }
}


void Client::resize(int W, int H) {
  aspect = float(W)/float(H);
  glViewport(0,0,W,H);
}


void Client::mousedrag(int x, int y, bool left_button) 
{
	Matrix r;
	if(left_button)
	{
		const float ROT_INC=0.5;
		u=normalize(cross(look,EYAXIS));
		Matrix R=rotate(y*ROT_INC,u);
		look=R*look;
		
		u=normalize(cross(look,u));
		 r=rotate(x*ROT_INC,u);
		look=r*look;
	}
}



int main(int argc, char *argv[]) {

 
  SDL_Init(SDL_INIT_VIDEO);
  const char *title = "Window Title";
  int width = 1000,
      height = 1000;
  SDL_Window *window = SDL_CreateWindow(title,SDL_WINDOWPOS_UNDEFINED,
                                        SDL_WINDOWPOS_UNDEFINED,width,height,
                                        SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
  SDL_GLContext context = SDL_GL_CreateContext(window);

 
  glewInit();
  if (!GLEW_VERSION_2_0) {
    cout << "needs OpenGL version 2.0 or better" << endl;
    return -1;
  }

  // animation loop
  bool done = false;
  Client *client = new Client();
  Uint32 ticks_last = SDL_GetTicks();
  while (!done) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          done = true;
          break;
        case SDL_KEYDOWN:
          if (event.key.keysym.sym == SDLK_ESCAPE)
            done = true;
          else
            client->keypress(event.key.keysym.sym);
          break;
        case SDL_WINDOWEVENT:
          if (event.window.event == SDL_WINDOWEVENT_RESIZED)
            client->resize(event.window.data1,event.window.data2);
          break;
        case SDL_MOUSEMOTION:
          if ((event.motion.state&SDL_BUTTON_LMASK) != 0
              || (event.motion.state&SDL_BUTTON_RMASK) != 0)
            client->mousedrag(event.motion.xrel,event.motion.yrel,
                              (event.motion.state&SDL_BUTTON_LMASK) != 0);
          break;
      }
    }
    Uint32 ticks = SDL_GetTicks();
    dt = 0.001*(ticks - ticks_last);
    ticks_last = ticks;
    client->draw(dt);
    SDL_GL_SwapWindow(window);
  }

  // clean up
  delete client;
  SDL_GL_DeleteContext(context);
  SDL_Quit();
  return 0;
}

