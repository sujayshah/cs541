// cs541texture-template.cpp
// -- template for assignment #4 (textures)
// cs541 10/17
//
// To compile from Visual Studio 2015 command prompt:
//    cl /EHsc cs541texture-template.cpp Affine3D.cpp Plane.cpp
//       opengl32.lib glew32.lib sdl2.lib sdl2main.lib
//       /link /subsystem:console
// To compile from Linux command line:
//    g++ cs541texture-template.cpp Affine3D.cpp Plane.cpp
//        -lGL -lGLEW -lSDL2

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <sstream>
#include "SDL2/SDL.h"
#include "GL/glew.h"
#include "GL/gl.h"
#include "Affine3D.h"
#include "Plane.h"
#include "Sphere.h"
#include <vector>


using namespace std;


const int MESH_SIZE = 20;
const float PI = 4.0f * atan(1.0f);
const int total_shapes = 3;
#define MATH_PI      3.1415926535897932384626433832795


const int FRAME_BUFFER_W = 800,
        FRAME_BUFFER_H = 800;

Hcoord eye;
Hcoord lookAt;

Hcoord EYEAXIS(0,1,0,0) ;
Hcoord Origin(0,0,0,0) , light_pos(0,0,0,0);

int fps = 0;
int fov  = 80;

GLfloat cube_vertices[] = {
        -1.0,  1.0,  1.0,
        -1.0, -1.0,  1.0,
        1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
};


GLushort cube_indices[] = {
        0, 1, 2, 3,
        3, 2, 6, 7,
        7, 6, 5, 4,
        4, 5, 1, 0,
        0, 3, 7, 4,
        1, 2, 6, 5,
};

// load a text file into a string
string loadTextFile(const char *fname) {
    string out,
            line;
    ifstream in(fname);
    getline(in, line);
    while (in) {
        out += line + "\n";
        getline(in, line);
    }
    return out;
}


// read a 24-bit color bitmap image file
//   Note: caller is responsible for deallocating the
//         RGB data
unsigned char *loadBitmapFile(const char *fname, int *width, int *height) {
    fstream in(fname, ios_base::in | ios_base::binary);
    char header[38];
    in.read(header, 38);
    unsigned offset = *reinterpret_cast<unsigned *>(header + 10);
    int W = *reinterpret_cast<int *>(header + 18),
            H = *reinterpret_cast<int *>(header + 22),
            S = 3 * W;
    S += (4 - S % 4) % 4;
    int size = S * H;
    in.seekg(offset, ios_base::beg);
    unsigned char *data = new unsigned char[size];
    in.read(reinterpret_cast<char *>(data), size);
    if (!in) {
        delete[] data;
        return 0;
    }

    for (int j = 0; j < H; ++j) {
        for (int i = 0; i < W; ++i) {
            int index = S * j + 3 * i;
            swap(data[index + 0], data[index + 2]);
        }
    }

    *width = W;
    *height = H;
    return data;
}


/////////////////////////////////////////////////////////////////
class Client {
public:
    Client(void);

    ~Client(void);


void drawfloor();
    void drawSphere();
    void loadSphere();
    void draw(double dt);
    void drawPlane(double dt);
    void renderdata();
    void drawToBuffer();
    void drawfromfbo();
    void keypress(SDL_Keycode kc);
    void drawskydome();
    void loadskydome();

    void deletePlaneBuffers();
    void loadPlaneTexture();
    void loadPlaneBuffers();
    void resize(int W, int H);

    void mousedrag(int x, int y, bool lbutton);


    void loadshader();
    void loadHelix();
    void drawHelix();

    void loadbuffers();

private:
    GLuint vertex_buffer[total_shapes],
            floor_buffer,
            floor_normal,buff,
            floor_face_buff,

            normal_buffer[total_shapes],
            face_buffer[total_shapes],
            texcoord_buffer[total_shapes],
            texture_buffer[total_shapes],
            text_floor_cord,
            text_floor_buffer,
            frame_buffer,//
            frame_buffer_texture_buffer,
            depth_buffer,
            cube_map_texture,
            sphere_vertex_buffer,
            sphere_normal,
            sphere_texture,
            sphere_texture_cords,
            sphere_face_buffer,
            sphere_face_count ,
            helix_normal,
            helix_vertex_buffer,

            skydome_vertex_buffer,
            skydome_texture,
            skydome_texture_cords,
            skydome_face_buff,
            sky_dome_face_count;


    GLint program,
            skyboxprogram,
            aposition,
            anormal,
            atexture_coord,
            upersp_matrix,
            uview_matrix,
            umodel_matrix,
            unormal_matrix,
            ulight_position,
            ulight_color,
            uambient_color,
            ucamera_position,
            ushine,
            uspecular_color,
            ushape_color,
            uis_shape,
            uambient_sky;

    int face_counts[2];
    int face_count;
    int floor_face_count;
    float time;
    float aspect;
};



void Client::loadskydome() {

    Plane plane(MESH_SIZE);
    Plane sphere(MESH_SIZE);

    sky_dome_face_count = plane.faceCount();

    glGenBuffers(1, &skydome_vertex_buffer);
    glGenBuffers(1,&skydome_face_buff);

    Matrix m  = scale(.5f , .5f , 1) * translate(Hcoord(1,1,0,0));
    Matrix n  = scale(PI , PI*.5f , 1) * translate(Hcoord(1,1,0,0));
    //Hcoord sphere_vertex;


    // float *tex_cords = new float[2*sphere.vertexCount()];
    float *vertex_cord = new float[4*sphere.vertexCount()];

    Hcoord *normals = new Hcoord[sphere.vertexCount()];


    for(int i = 0  ; i < sphere.vertexCount();i++) {

        Hcoord st = n*sphere.vertexArray()[i];
        sphere.vertexArray()[i].x = -sinf(st[1])*cosf(st[0]);
        sphere.vertexArray()[i].z = -sinf(st[1])*sinf(st[0]);
        sphere.vertexArray()[i].y = -cosf(st[1]);
  }

//    int vertex_buffer_size = 4* sizeof(float) * sphere.vertexCount();//
    int vertex_buffer_size =  sizeof(Hcoord) * sphere.vertexCount();
    glBindBuffer(GL_ARRAY_BUFFER , skydome_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, sphere.vertexArray(), GL_STATIC_DRAW);


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skydome_face_buff);
    int face_buffer_size = sizeof(Mesh3D::Face) * sphere.faceCount();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, face_buffer_size, sphere.faceArray(), GL_STATIC_DRAW);




    int width, height;
    unsigned char *rgbdata = loadBitmapFile("polardome.bmp", &width, &height);
    glGenTextures(1, &skydome_texture);
    glBindTexture(GL_TEXTURE_2D, skydome_texture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbdata);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


    glGenBuffers(1, &skydome_texture_cords);
    glBindBuffer(GL_ARRAY_BUFFER, skydome_texture_cords);
    float *texcoord = new float[2 * plane.vertexCount()];
    Matrix Std2Unit = scale(.5f, .5f, 1)
                      * translate(Hcoord(1, 1, 0, 0));
    for (int i = 0; i < plane.vertexCount(); ++i) {
        Hcoord uv = Std2Unit * plane.vertexArray()[i];
        texcoord[2 * i + 0] = uv[0];
        texcoord[2 * i + 1] = uv[1];
    }

    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * plane.vertexCount(), texcoord, GL_STATIC_DRAW);

    delete[] texcoord;
    delete []vertex_cord;
}
//textured sphere
void Client::loadSphere()
 {


    Plane plane(MESH_SIZE);
    Plane sphere(MESH_SIZE);

    unsigned  int face_count_ = plane.faceCount();

    glGenBuffers(1, &sphere_vertex_buffer);
    glGenBuffers(1, &sphere_normal);
  //  glGenBuffers(1, &sphere_face_buffer);

    Matrix m  = scale(.5f , .5f , 1) * translate(Hcoord(1,1,0,0));
    Matrix n  = scale(PI , PI*.5f , 1) * translate(Hcoord(1,1,0,0));
    //Hcoord sphere_vertex;

    sphere_face_count = sphere.vertexCount();

   // float *tex_cords = new float[2*sphere.vertexCount()];
    float *vertex_cord = new float[4*sphere.vertexCount()];

    Hcoord *normals = new Hcoord[sphere.vertexCount()];


     for(int i = 0  ; i < sphere.vertexCount();i++) 
     {

        /* Hcoord uv = m * sphere.vertexArray()[i];
         tex_cords[2*i+0] = uv[0];
         tex_cords[2*i+1] = uv[1];*/

        /* Hcoord st = n*sphere.vertexArray()[i];
         sphere.vertexArray()[i].x = sinf(st[0])*cosf(st[1]);
         sphere.vertexArray()[i].z = -sinf(st[0])*sinf(st[1]);
         sphere.vertexArray()[i].y = -cosf(st[1]);

*/


         Hcoord st = n*sphere.vertexArray()[i];
         sphere.vertexArray()[i].x = sinf(st[1])*cosf(st[0]);
         sphere.vertexArray()[i].z = -sinf(st[1])*sinf(st[0]);
         sphere.vertexArray()[i].y = -cosf(st[1]);
//         vertex_cord[4*i+3] = 1;

         normals[i] = Hcoord(sphere.vertexArray()[i].x ,sphere.vertexArray()[i].y,sphere.vertexArray()[i].z,0);
      }

//    int vertex_buffer_size = 4* sizeof(float) * sphere.vertexCount();//
    int vertex_buffer_size =  sizeof(Hcoord) * sphere.vertexCount();
    glBindBuffer(GL_ARRAY_BUFFER , sphere_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, sphere.vertexArray(), GL_STATIC_DRAW);


    int normalsize = sizeof(Hcoord)*sphere.vertexCount();
    glBindBuffer(GL_ARRAY_BUFFER, sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, normalsize/*vertex_buffer_size*/, normals, GL_STATIC_DRAW);


    int width, height;
    unsigned char *rgbdata = loadBitmapFile("polardome.bmp", &width, &height);
    glGenTextures(1, &sphere_texture);
    glBindTexture(GL_TEXTURE_2D, sphere_texture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbdata);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


    glGenBuffers(1, &sphere_texture_cords);
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);

    float *texcoord = new float[2 * plane.vertexCount()];
    Matrix Std2Unit = scale(.5f, .5f, 1) * translate(Hcoord(1, 1, 0, 0));
    for (int i = 0; i < plane.vertexCount(); ++i) {
        Hcoord uv = Std2Unit * plane.vertexArray()[i];
        texcoord[2 * i + 0] = uv[0];
        texcoord[2 * i + 1] = uv[1];
    }
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * plane.vertexCount(), texcoord, GL_STATIC_DRAW);

    delete[] texcoord;
    delete [] normals;
    delete []vertex_cord;

    loadHelix();
}


void Client::loadHelix() {



    Plane plane(MESH_SIZE);
    Plane sphere(MESH_SIZE);

    unsigned  int face_count_ = plane.faceCount();

    glGenBuffers(1, &helix_vertex_buffer);
    glGenBuffers(1, &helix_normal);
    //  glGenBuffers(1, &sphere_face_buffer);

    Matrix m  = scale(.5f , .5f , 1) * translate(Hcoord(1,1,0,0));
    Matrix n  = scale(PI , PI*.5f , 1) * translate(Hcoord(1,1,0,0));
    //Hcoord sphere_vertex;

    sphere_face_count = sphere.vertexCount();

    // float *tex_cords = new float[2*sphere.vertexCount()];
    float *vertex_cord = new float[4*sphere.vertexCount()];

    Hcoord *normals = new Hcoord[sphere.vertexCount()];


    for(int i = 0  ; i < sphere.vertexCount();i++) {

        /* Hcoord uv = m * sphere.vertexArray()[i];
         tex_cords[2*i+0] = uv[0];
         tex_cords[2*i+1] = uv[1];*/

        Hcoord st = n*sphere.vertexArray()[i];
        sphere.vertexArray()[i].x = sinf(st[0])*cosf(st[1]);
        sphere.vertexArray()[i].z = -sinf(st[0])*sinf(st[1]);
        sphere.vertexArray()[i].y = -cosf(st[1]);



/*
         Hcoord st = n*sphere.vertexArray()[i];
         sphere.vertexArray()[i].x = sinf(st[1])*cosf(st[0]);
         sphere.vertexArray()[i].z = -sinf(st[1])*sinf(st[0]);
         sphere.vertexArray()[i].y = -cosf(st[1]);*/
//         vertex_cord[4*i+3] = 1;

        normals[i] = Hcoord(sphere.vertexArray()[i].x ,sphere.vertexArray()[i].y,sphere.vertexArray()[i].z,0);
    }

//    int vertex_buffer_size = 4* sizeof(float) * sphere.vertexCount();//
    int vertex_buffer_size =  sizeof(Hcoord) * sphere.vertexCount();
    glBindBuffer(GL_ARRAY_BUFFER , helix_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, sphere.vertexArray(), GL_STATIC_DRAW);


    int normalsize = sizeof(Hcoord)*sphere.vertexCount();
    glBindBuffer(GL_ARRAY_BUFFER, helix_normal);
    glBufferData(GL_ARRAY_BUFFER, normalsize/*vertex_buffer_size*/, normals, GL_STATIC_DRAW);

/*
    int width, height;
    unsigned char *rgbdata = loadBitmapFile("polardome.bmp", &width, &height);
    glGenTextures(1, &sphere_texture);
    glBindTexture(GL_TEXTURE_2D, sphere_texture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbdata);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


    glGenBuffers(1, &sphere_texture_cords);
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);

    float *texcoord = new float[2 * plane.vertexCount()];
    Matrix Std2Unit = scale(.5f, .5f, 1) * translate(Hcoord(1, 1, 0, 0));
    for (int i = 0; i < plane.vertexCount(); ++i) {
        Hcoord uv = Std2Unit * plane.vertexArray()[i];
        texcoord[2 * i + 0] = uv[0];
        texcoord[2 * i + 1] = uv[1];
    }
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * plane.vertexCount(), texcoord, GL_STATIC_DRAW);

    delete[] texcoord;*/
    delete [] normals;
    delete []vertex_cord;



}


void Client::drawHelix() {

    const float RATE = 360.0f / 5.0f;
    const Hcoord AXIS(0, 1, 0, 0),
            CENTER(2, 1.6, -3, 0);
    Matrix M = translate(CENTER)
               * rotate(RATE * time, AXIS)
               * scale(.5);
    glUniformMatrix4fv(umodel_matrix, 1, true, (float *) &M);
    glUniformMatrix4fv(unormal_matrix, 1, true, (float *) &M);


    // set shader attributes
    glBindBuffer(GL_ARRAY_BUFFER, helix_vertex_buffer);
    glEnableVertexAttribArray(aposition);
    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, helix_normal);
    glEnableVertexAttribArray(anormal);
    glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);


    //texture
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

    glBindTexture(GL_TEXTURE_2D, sphere_texture);
    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer[0]);
    glDrawElements(GL_TRIANGLES,3*face_counts[0],GL_UNSIGNED_INT,0);

}



void Client::drawSphere() {

    ///  glUseProgram(skyboxprogram);
    const float RATE = 360.0f / 5.0f;
    const Hcoord AXIS(0, 1, 0, 0),
            CENTER(0, 1.6, -3, 0);
    Matrix M = translate(CENTER)
               * rotate(RATE * time, AXIS)
               * scale(.5);
    glUniformMatrix4fv(umodel_matrix, 1, true, (float *) &M);
    glUniformMatrix4fv(unormal_matrix, 1, true, (float *) &M);


    // set shader attributes
    glBindBuffer(GL_ARRAY_BUFFER, sphere_vertex_buffer);
    glEnableVertexAttribArray(aposition);
    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, sphere_normal);
    glEnableVertexAttribArray(anormal);
    glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);


    //texture
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

    glBindTexture(GL_TEXTURE_2D, sphere_texture);
    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer[0]);
    glDrawElements(GL_TRIANGLES,3*face_counts[0],GL_UNSIGNED_INT,0);

    drawHelix();


/*
    const float RATE = 360.0f/5.0f;

    const Hcoord AXIS(0,0,0,1),
            pos(0,0,2, 0);
    Matrix R = rotate(1,AXIS),
            M = translate(pos)
                * R
                * scale(.5);
    glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
    glUniformMatrix4fv(unormal_matrix,1,true,(float*)&R);

    glBindBuffer(GL_ARRAY_BUFFER,sphere_vertex_buffer);
    glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
    glEnableVertexAttribArray(aposition);

    glBindBuffer(GL_ARRAY_BUFFER,sphere_normal);
    glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
    glEnableVertexAttribArray(anormal);

    //texture
    glBindBuffer(GL_ARRAY_BUFFER, sphere_texture_cords);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,sphere_face_buffer);
    glDrawElements(GL_TRIANGLES,3*sphere_face_count,GL_UNSIGNED_INT,0);


    glBindTexture(GL_TEXTURE_2D, sphere_texture);
    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphere_face_buffer);
    glDrawElements(GL_TRIANGLES, 3 *  face_counts[0], GL_UNSIGNED_INT, 0);*/


}

void Client::drawskydome() {


    aposition = glGetAttribLocation(skyboxprogram, "position");
   
    upersp_matrix = glGetUniformLocation(skyboxprogram, "persp_matrix");
    uview_matrix = glGetUniformLocation(skyboxprogram, "view_matrix");
    umodel_matrix = glGetUniformLocation(skyboxprogram, "model_matrix");
    uambient_color = glGetUniformLocation(skyboxprogram, "ambient_color");
  ucamera_position = glGetUniformLocation(skyboxprogram,"camera_position");
    uambient_sky = glGetUniformLocation(skyboxprogram , "ambient_color");

   // glUniform3f(uambient_sky, .5,.5,.5);

  Matrix P = perspective(fov,aspect,0.1f,20.0f);
    glUniformMatrix4fv(upersp_matrix,1,true,(float*)&P);
    Matrix V = scale(1);

    V.rows[0] = normalize(cross(lookAt , EYEAXIS));
    V.rows[2] = -normalize(lookAt);
    V.rows[1] = cross(V.rows[2] , V.rows[0]);

    V = V* translate(Origin-eye);

    glUniformMatrix4fv(uview_matrix,1,true,(float*)&V);



  ///  glUseProgram(skyboxprogram);
    const float RATE = 360.0f / 5.0f;
    const Hcoord AXIS(0, 1, 0, 0),
            CENTER(eye.x, eye.y, eye.z, 0);
    Matrix M = translate(CENTER)
               * rotate(0, AXIS)
               * scale(20);
    glUniformMatrix4fv(umodel_matrix, 1, true, (float *) &M);
    glUniformMatrix4fv(unormal_matrix, 1, true, (float *) &M);


    // set shader attributes
    glBindBuffer(GL_ARRAY_BUFFER, skydome_vertex_buffer);
    glEnableVertexAttribArray(aposition);
    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);


    glBindBuffer(GL_ARRAY_BUFFER, skydome_texture_cords);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

    glBindTexture(GL_TEXTURE_2D, skydome_texture);
    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skydome_face_buff);

    glDrawElements(GL_TRIANGLES,3*sky_dome_face_count,GL_UNSIGNED_INT,0);


}

void Client::loadshader() {

    {
        GLint value;
        string fragtextstr = loadTextFile("diffuse_texture.frag");
        const char *fragtext = fragtextstr.c_str();
        GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fshader, 1, &fragtext, 0);
        glCompileShader(fshader);
        glGetShaderiv(fshader, GL_COMPILE_STATUS, &value);
        if (!value) {
            cerr << "fragment shader failed to compile" << endl;
            char buffer[1024];
            glGetShaderInfoLog(fshader, 1024, 0, buffer);
            cerr << buffer << endl;
        }

        // (I.B) compile vertex shader
        string verttextstr = loadTextFile("diffuse_texture.vert").c_str();
        const char *verttext = verttextstr.c_str();
        GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vshader, 1, &verttext, 0);
        glCompileShader(vshader);
        glGetShaderiv(vshader, GL_COMPILE_STATUS, &value);
        if (!value) {
            cerr << "vertex shader failed to compile" << endl;
            char buffer[1024];
            glGetShaderInfoLog(vshader, 1024, 0, buffer);
            cerr << buffer << endl;
        }

        // (I.C) link shaders
        program = glCreateProgram();
        glAttachShader(program, fshader);
        glAttachShader(program, vshader);
        glLinkProgram(program);
        glGetProgramiv(program, GL_LINK_STATUS, &value);
        if (!value) {
            cerr << "vertex shader failed to compile" << endl;
            char buffer[1024];
            glGetShaderInfoLog(vshader, 1024, 0, buffer);
            cerr << buffer << endl;
        }
        glDeleteShader(fshader);
        glDeleteShader(vshader);
    }
///////////////////////////////////////////////
    {


        GLint value;
        string fragtextstr = loadTextFile("skybox.frag");
        const char *fragtext = fragtextstr.c_str();
        GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fshader, 1, &fragtext, 0);
        glCompileShader(fshader);
        glGetShaderiv(fshader, GL_COMPILE_STATUS, &value);
        if (!value) {
            cerr << "fragment shader failed to compile" << endl;
            char buffer[1024];
            glGetShaderInfoLog(fshader, 1024, 0, buffer);
            cerr << buffer << endl;
        }

        // (I.B) compile vertex shader
        string verttextstr = loadTextFile("diffuse_texture.vert").c_str();
        const char *verttext = verttextstr.c_str();
        GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vshader, 1, &verttext, 0);
        glCompileShader(vshader);
        glGetShaderiv(vshader, GL_COMPILE_STATUS, &value);
        if (!value) {
            cerr << "vertex shader failed to compile" << endl;
            char buffer[1024];
            glGetShaderInfoLog(vshader, 1024, 0, buffer);
            cerr << buffer << endl;
        }

        // (I.C) link shaders
        skyboxprogram = glCreateProgram();
        glAttachShader(skyboxprogram, fshader);
        glAttachShader(skyboxprogram, vshader);
        glLinkProgram(skyboxprogram);
        glGetProgramiv(skyboxprogram, GL_LINK_STATUS, &value);
        if (!value) {
            cerr << "vertex shader failed to compile" << endl;
            char buffer[1024];
            glGetShaderInfoLog(vshader, 1024, 0, buffer);
            cerr << buffer << endl;
        }
        glDeleteShader(fshader);
        glDeleteShader(vshader);

    }

}

void Client::loadbuffers() {


    loadSphere();
    loadskydome();

    loadPlaneBuffers();

    Plane plane(MESH_SIZE);
    Sphere sphere(MESH_SIZE);


    face_counts[0] = plane.faceCount();
    face_counts[1] = sphere.faceCount();
    std::cout<<"Plane has face count of : "<<plane.faceCount()<<std::endl;

    glGenBuffers(3, vertex_buffer);
    glGenBuffers(3, normal_buffer);
    glGenBuffers(3, face_buffer);


    face_buffer[2] = face_buffer[0];
    vertex_buffer[2] = vertex_buffer[0];
    normal_buffer[2] = normal_buffer[0];


//plane
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[0]);
    int vertex_buffer_size = sizeof(Hcoord) * plane.vertexCount();
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, plane.vertexArray(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer[0]);
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, plane.normalArray(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, face_buffer[0]);
    int face_buffer_size = sizeof(Mesh3D::Face) * face_counts[1];
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, face_buffer_size, plane.faceArray(), GL_STATIC_DRAW);


    //sphere
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[1]);
    vertex_buffer_size = sizeof(Hcoord) * sphere.vertexCount();
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, sphere.vertexArray(), GL_STATIC_DRAW);


    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer[1]);
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, sphere.normalArray(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, face_buffer[1]);
    face_buffer_size = sizeof(Mesh3D::Face) *  face_counts[1];
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, face_buffer_size, sphere.faceArray(), GL_STATIC_DRAW);


    //texture
    // (III.D) texture coordinate buffer
    glGenBuffers(2, texcoord_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, texcoord_buffer[0]);
    float *texcoord = new float[2 * plane.vertexCount()];
    Matrix Std2Unit = scale(1.5f, 1.5f, 1)
                      * translate(Hcoord(1, 1, 0, 0));
    for (int i = 0; i < plane.vertexCount(); ++i) {
        Hcoord uv = Std2Unit * plane.vertexArray()[i];
        texcoord[2 * i + 0] = uv[0];
        texcoord[2 * i + 1] = uv[1];
    }
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * plane.vertexCount(), texcoord, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, texcoord_buffer[1]);
    texcoord = new float[2 * plane.vertexCount()];
    Std2Unit = scale(0.5f, 0.5f, 1)
               * translate(Hcoord(1, 1, 0, 0));
    for (int i = 0; i < plane.vertexCount(); ++i) {
        Hcoord uv = Std2Unit * plane.vertexArray()[i];
        texcoord[2 * i + 0] = uv[0];
        texcoord[2 * i + 1] = uv[1];
    }
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * plane.vertexCount(), texcoord, GL_STATIC_DRAW);


    delete[] texcoord;

}

void Client:: loadPlaneTexture() {


    int width, height;
    glBindTexture(GL_TEXTURE_2D, 0);
    unsigned char *rgbdata1 = loadBitmapFile("floor2.bmp", &width, &height);

    glGenTextures(1 , &text_floor_buffer);
    glBindTexture(GL_TEXTURE_2D, text_floor_buffer);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbdata1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

}

void Client::loadPlaneBuffers() {


    Plane floor(MESH_SIZE*MESH_SIZE);

    //floor
    // (III.A) vertex buffer
    glGenBuffers(1 , &floor_buffer);
    glBindBuffer(GL_ARRAY_BUFFER , floor_buffer);
     int vsize = sizeof(Hcoord) * floor.vertexCount();
     glBufferData(GL_ARRAY_BUFFER, vsize, floor.vertexArray(), GL_STATIC_DRAW);

     glGenBuffers(1 , &floor_normal);
     glBindBuffer(GL_ARRAY_BUFFER, floor_normal);
     glBufferData(GL_ARRAY_BUFFER, vsize, floor.normalArray(), GL_STATIC_DRAW);

     glGenBuffers(1 , &floor_face_buff);
     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, floor_face_buff);
     int f = sizeof(Mesh3D::Face) * floor.faceCount();
     floor_face_count =  floor.faceCount();
     glBufferData(GL_ELEMENT_ARRAY_BUFFER, f, floor.faceArray(), GL_STATIC_DRAW);


    glGenBuffers(1, &text_floor_cord);
    glBindBuffer(GL_ARRAY_BUFFER, text_floor_cord);
    float *texcoord = new float[2 * floor.vertexCount()];
    Matrix Std2Unit = scale(2.5f, 2.5f, 1)
               * translate(Hcoord(1, 1, 0, 0));
    for (int i = 0; i < floor.vertexCount(); ++i) {
        Hcoord uv = Std2Unit * floor.vertexArray()[i];
        texcoord[2 * i + 0] = uv[0];
        texcoord[2 * i + 1] = uv[1];
    }
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float) * floor.vertexCount(), texcoord, GL_STATIC_DRAW);


    delete[] texcoord;


}

void Client::drawToBuffer() {

    ///////////////////////////////////////////////////////////////
    // pass #1: render to frame buffer (use texture from file)
    ///////////////////////////////////////////////////////////////
    glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer);

    glBindTexture(GL_TEXTURE_2D, texture_buffer[1]);
    glBindBuffer(GL_ARRAY_BUFFER, texcoord_buffer[1]);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);


    int viewport_save[4];
    glGetIntegerv(GL_VIEWPORT,viewport_save);
    glViewport(0,0,FRAME_BUFFER_W,FRAME_BUFFER_H);

    glClearColor(.1f,0.1f,0.1f,1);
    glClearDepth(1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    drawPlane(.1);
     glUniform1i(uis_shape , 0);
/////////////


   /* Matrix P = perspective(fov,aspect,0.1f,20.0f);
    glUniformMatrix4fv(upersp_matrix,1,true,(float*)&P);
    Matrix V = scale(1);

    V.rows[0] = normalize(cross(lookAt , EYEAXIS));
    V.rows[2] = -normalize(lookAt);
    V.rows[1] = cross(V.rows[2] , V.rows[0]);

    V = V* translate(Origin-eye);

    glUniformMatrix4fv(uview_matrix,1,true,(float*)&V);*/

    //////////////

    // draw using texture from file
    const float RATE = 360.0f/5.0f;
    const Hcoord AXIS(0,1,0,0),
            CENTER(0,0,-3,0);
    Matrix M = translate(CENTER)
               * rotate(RATE*time,AXIS)
               * scale(1.5f);
    glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
    glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);
    glBindTexture(GL_TEXTURE_2D,texture_buffer[0]);
    glDrawElements(GL_TRIANGLES,3 * face_counts[0],GL_UNSIGNED_INT,0);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
}

void Client::drawfromfbo() {
    const float RATE = 360.0f/5.0f;
    // draw using frame buffer texture
    const Hcoord EZ(0,0,1,0);
    Matrix M = translate(Hcoord(0.75f,-0.75f,-2,0))
               * rotate(-0.5f*RATE*time,EZ)
               * scale(0.5f);
    glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
    glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);
    glBindTexture(GL_TEXTURE_2D,frame_buffer_texture_buffer);
    glDrawElements(GL_TRIANGLES,3*face_counts[0],GL_UNSIGNED_INT,0);
}
Client::Client(void) {

    loadbuffers();
    loadshader();

    // (II) get shader variable locations
    aposition = glGetAttribLocation(program, "position");
    anormal = glGetAttribLocation(program, "normal");
    atexture_coord = glGetAttribLocation(program, "texture_coord");
    upersp_matrix = glGetUniformLocation(program, "persp_matrix");
    uview_matrix = glGetUniformLocation(program, "view_matrix");
    umodel_matrix = glGetUniformLocation(program, "model_matrix");
    unormal_matrix = glGetUniformLocation(program, "normal_matrix");
    ulight_position = glGetUniformLocation(program, "light_position");
    ulight_color = glGetUniformLocation(program, "light_color");
    uambient_color = glGetUniformLocation(program, "ambient_color");
    ucamera_position = glGetUniformLocation(program,"camera_position");
    ushine = glGetUniformLocation(program,"shine");
    uspecular_color = glGetUniformLocation(program , "specular_color");
    ushape_color =  glGetUniformLocation(program , "shape_color");
    uis_shape = glGetUniformLocation(program , "is_shape");

    uambient_sky = glGetUniformLocation(skyboxprogram , "ambient_color");



    // (IV) upload texture to GPU
    int width, height;
    unsigned char *rgbdata = loadBitmapFile("img.bmp", &width, &height);
    glGenTextures(2, texture_buffer);
    glBindTexture(GL_TEXTURE_2D, texture_buffer[0]);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbdata);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);



    glBindTexture(GL_TEXTURE_2D, 0);
    unsigned char *rgbdata1 = loadBitmapFile("img1.bmp", &width, &height);
    glBindTexture(GL_TEXTURE_2D, texture_buffer[1]);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgbdata1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


    loadPlaneTexture();

///////////////////////////////////////////////

    // create a frame buffer object to use a texture
    // (1) frame buffer
    glGenFramebuffers(1,&frame_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER,frame_buffer);
    // (2) texture associated to frame buffer
    glGenTextures(1,&frame_buffer_texture_buffer);
    glBindTexture(GL_TEXTURE_2D,frame_buffer_texture_buffer);
    glPixelStorei(GL_UNPACK_ALIGNMENT,4);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,FRAME_BUFFER_W,FRAME_BUFFER_H,0,GL_RGB,GL_UNSIGNED_BYTE,0);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);*/

    // (3) depth buffer
    glGenRenderbuffers(1,&depth_buffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depth_buffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,1024,1024);
    // (4) attach depth buffer to frame buffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depth_buffer);
    // (5) attach texture to frame buffer
    glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,frame_buffer_texture_buffer,0);
    // (6) make sure everything is ok
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        cout << "frame buffer failure" << endl;
    ///////////////////////////////////////////////

    // (V) enable use of z-buffer
    glEnable(GL_DEPTH_TEST);

    glUseProgram(program);

    //shader attribs
    glUniform3f(ulight_color,1,1,1);
    glUniform3f(uambient_color,.5,.5,.5);
    glUniform1f(ushine , 32);
    glUniform3f(uspecular_color , 1,1,1);
    glUniform3f(uambient_sky, .5,.5,.5);


    light_pos =  Hcoord (1, 0.5f, 0, 1);


    eye = Hcoord(0,0,0,1);
    lookAt = Hcoord(0,0,-1,0);

    aspect = 1;
    time = 0;
}


Client::~Client(void) {
    // delete the shader program
    glUseProgram(0);
    glDeleteProgram(program);

    // delete the vertex, normal, texture coordinate, and face buffers
    glDeleteBuffers(2, face_buffer);
    glDeleteBuffers(2, normal_buffer);
    glDeleteBuffers(2, vertex_buffer);
    glDeleteBuffers(2, texcoord_buffer);

    // delete texture buffer
    glDeleteTextures(2, texture_buffer);


    glDeleteBuffers(1, &helix_normal);
    glDeleteBuffers(1, &helix_vertex_buffer);

    deletePlaneBuffers();


    glDeleteBuffers(1, &skydome_vertex_buffer);
    glDeleteBuffers(1, &skydome_texture_cords);
    glDeleteBuffers(1,&skydome_face_buff);



    glDeleteBuffers(1, &sphere_texture_cords);
    glDeleteBuffers(1, &sphere_vertex_buffer);
   // glDeleteBuffers(1, &sphere_face_buffer);
    glDeleteBuffers(1, &sphere_normal);
    glDeleteTextures(1,&sphere_texture);


    glDeleteTextures(1,&skydome_texture);


    // delete frame buffer
    glDeleteRenderbuffers(1,&depth_buffer);
    glDeleteTextures(1,&frame_buffer_texture_buffer);
    glDeleteFramebuffers(1,&frame_buffer);

}

void Client::deletePlaneBuffers() {



   glDeleteBuffers(1, &floor_buffer);
    glDeleteBuffers(1, &floor_normal);
    glDeleteBuffers(1, &floor_face_buff);
   // glDeleteBuffers(1, &text_floor_buffer);
    glDeleteBuffers(1, &text_floor_cord);

    glDeleteTextures(1,&text_floor_buffer);

}

void Client::drawPlane(double dt) {



    float x_itr = 0.0f;
    float y_itr = 0.0f;

    float n = 24.0f;
    float rad = (360.f/n )*(MATH_PI/180.0f);
    float start_x = -3.1f;
    float start_y = -3.0f;
    float offset = .44f;
    float r  =0.0;
    float g  =0.0;
    float b  =0.0;

    float _x = 0.0f;
    float _y = 0.0f;
    float _z = 0.0f;
    float z = -4;
    int x_itr1 = 0;
    float _scale = .15;


    for(float i = 0 ; i  < 50 ; i++) {

        if(y_itr >10 || (i >10 && i <25)) {

            y_itr = 0.0f;
            x_itr++;

            start_y = -3.0f;
        }

        if(i>=35) {

            _x = -3 + .25*x_itr1++;
            _y= 2.5+sin(time*(i/10));
            _z = -4;

            r = .7;
            g = .5;
            b = .7;
            x_itr1++;
            _scale = .1;
        } else {

            r =  (rand() % 100 + 1 ) / 100;
            g =  (rand() % 100 + 1 ) / 100;
            b = (rand() % 100 + 1 ) / 100;
            _x = start_x+offset*x_itr ;
            _y = start_y+offset*y_itr;
            _z = -4;
        }

        const float RATE = 360.0f/5.0f;

        const Hcoord AXIS(0,0,0,1),
                pos(_x,_y,_z, 0);
        Matrix R = rotate(1,AXIS),
                M = translate(pos)
                    * R
                    * scale(_scale);
        glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
        glUniformMatrix4fv(unormal_matrix,1,true,(float*)&R);


        // glUniform1f(ushine , 1);
        glUniform1i(uis_shape , 1);
        glUniform3f(ushape_color,0.1f, 0.1f, 0.1f);


        // set shader attributes
        glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer[2]);
        glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
        glEnableVertexAttribArray(aposition);

        glBindBuffer(GL_ARRAY_BUFFER,normal_buffer[2]);
        glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
        glEnableVertexAttribArray(anormal);

        // draw the mesh
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer[2]);
        glDrawElements(GL_TRIANGLES,3*face_counts[0],GL_UNSIGNED_INT,0);
        rad++;

        y_itr++;
    }
}

void Client::draw(double dt) {


    glViewport(0,0,FRAME_BUFFER_W,FRAME_BUFFER_H);
    // clear frame buffer and z-buffer
    glClearColor(0.2f, 0.2f, 0.2f, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glClearDepth(1);
    glClear(GL_DEPTH_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D,0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    //light related

      glUseProgram(skyboxprogram);
    /*glUniform4f(ulight_position, light_pos.x,light_pos.y,light_pos.z,0);
    glUniform4f(ucamera_position,eye.x,eye.y,eye.z,0);

*/
    drawskydome();

    {
           //glUseProgram(0);
      glUseProgram(program);


        aposition = glGetAttribLocation(program, "position");
        anormal = glGetAttribLocation(program, "normal");
        atexture_coord = glGetAttribLocation(program, "texture_coord");
        upersp_matrix = glGetUniformLocation(program, "persp_matrix");
        uview_matrix = glGetUniformLocation(program, "view_matrix");
        umodel_matrix = glGetUniformLocation(program, "model_matrix");
        unormal_matrix = glGetUniformLocation(program, "normal_matrix");
        ulight_position = glGetUniformLocation(program, "light_position");
        ulight_color = glGetUniformLocation(program, "light_color");
        uambient_color = glGetUniformLocation(program, "ambient_color");
        ucamera_position = glGetUniformLocation(program,"camera_position");
        ushine = glGetUniformLocation(program,"shine");
        uspecular_color = glGetUniformLocation(program , "specular_color");
        ushape_color =  glGetUniformLocation(program , "shape_color");
        uis_shape = glGetUniformLocation(program , "is_shape");

        glUniform1i(uis_shape , 0);
    }
 
    glUniform3f(ulight_color,1,1,1);
    glUniform3f(uambient_color,.5,.5,.5);
    glUniform1f(ushine , 32);
    glUniform3f(uspecular_color , 1,1,1);


    glBindTexture(GL_TEXTURE_2D,0);
    drawSphere();
    glBindTexture(GL_TEXTURE_2D,0);

    //glBindBuffer(GL_ARRAY_BUFFER,0);
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    drawPlane(dt);
    glUniform1i(uis_shape , 0);

    //
//////////////////////////////////



    Matrix P = perspective(fov,aspect,0.1f,20.0f);
    glUniformMatrix4fv(upersp_matrix,1,true,(float*)&P);
    Matrix V = scale(1);

    V.rows[0] = normalize(cross(lookAt , EYEAXIS));
    V.rows[2] = -normalize(lookAt);
    V.rows[1] = cross(V.rows[2] , V.rows[0]);

    V = V* translate(Origin-eye);

    glUniformMatrix4fv(uview_matrix,1,true,(float*)&V);

glUniform4f(ulight_position, light_pos.x,light_pos.y,light_pos.z,0);

    //////////////////////////////////////////
    const float RATE = 360.0f / 5.0f;
    const Hcoord AXIS(0, 1, 0, 0),
            CENTER(0, 0, -3, 0);
    Matrix M = translate(CENTER)
               * rotate(RATE * time, AXIS)
               * scale(1);
    glUniformMatrix4fv(umodel_matrix, 1, true, (float *) &M);
    glUniformMatrix4fv(unormal_matrix, 1, true, (float *) &M);


    // set shader attributes
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer[0]);
    glEnableVertexAttribArray(aposition);
    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normal_buffer[0]);
    glEnableVertexAttribArray(anormal);
    glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);


    //texture
    glBindBuffer(GL_ARRAY_BUFFER, texcoord_buffer[0]);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

    glBindTexture(GL_TEXTURE_2D, texture_buffer[1]);
    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, face_buffer[0]);
    glDrawElements(GL_TRIANGLES, 3 *  face_counts[0], GL_UNSIGNED_INT, 0);



    drawfloor();

    drawToBuffer();
    drawfromfbo();
    time += dt;
}

void Client::drawfloor() {


    const float RATE = 360.0f / 5.0f;
    const Hcoord AXIS(1, 0, 0, 0),
            CENTER(0, -5, 0, 0);
    Matrix M = translate(CENTER)
               * rotate(-90, AXIS)
               * scale(20);
    glUniformMatrix4fv(umodel_matrix, 1, true, (float *) &M);
    glUniformMatrix4fv(unormal_matrix, 1, true, (float *) &M);


    // set shader attributes
    glBindBuffer(GL_ARRAY_BUFFER, floor_buffer);
    glEnableVertexAttribArray(aposition);
    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, floor_normal);
    glEnableVertexAttribArray(anormal);
    glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);


    //texture
    glBindBuffer(GL_ARRAY_BUFFER, text_floor_cord);
    glEnableVertexAttribArray(atexture_coord);
    glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

    glBindTexture(GL_TEXTURE_2D, text_floor_buffer);
    // draw the mesh
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, floor_face_buff);
    glDrawElements(GL_TRIANGLES, 3 *  floor_face_count, GL_UNSIGNED_INT, 0);
    {


        const float RATE = 360.0f / 5.0f;
        const Hcoord AXIS(1, 0, 0, 0),
                CENTER(-5, -5, 0, 0);
        Matrix M = translate(CENTER)
                   * rotate(-90, AXIS)
                   * scale(20);
        glUniformMatrix4fv(umodel_matrix, 1, true, (float *) &M);
        glUniformMatrix4fv(unormal_matrix, 1, true, (float *) &M);


        // set shader attributes
        glBindBuffer(GL_ARRAY_BUFFER, floor_buffer);
        glEnableVertexAttribArray(aposition);
        glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, floor_normal);
        glEnableVertexAttribArray(anormal);
        glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);


        //texture
        glBindBuffer(GL_ARRAY_BUFFER, text_floor_cord);
        glEnableVertexAttribArray(atexture_coord);
        glVertexAttribPointer(atexture_coord, 2, GL_FLOAT, false, 0, 0);

        glBindTexture(GL_TEXTURE_2D, text_floor_buffer);
        // draw the mesh
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, floor_face_buff);
        glDrawElements(GL_TRIANGLES, 3 *  floor_face_count, GL_UNSIGNED_INT, 0);


    }


}

void Client::renderdata() {


}
void Client::keypress(SDL_Keycode kc) {

    const float speed = .1;


    Hcoord u = normalize(cross(lookAt,EYEAXIS));
    Matrix R;


    switch(kc) {

        case SDLK_d:

            eye = eye+ speed*u;

            break;

        case SDLK_a:

            eye = eye - speed*u;
            break;

        case  SDLK_w:

            eye = eye + speed*lookAt;
            break;

        case  SDLK_s:

            eye = eye - speed*lookAt;
            break;

 case SDLK_k:
        
        light_pos.z += 1;
        
      break;
      
      case SDLK_j:
      
        light_pos.x -= 2;
      break;
      
      case  SDLK_i:
      
        light_pos.z -= 2;
      break;
      
      case  SDLK_l:

        light_pos.x += 2;
      break;


        case SDLK_UP:

            /*R= rotate(speed , u);
           lookAt = R*lookAt;*/

            fov-=10;
            fov = fov <30 ? 30 :fov;

            break;


        case SDLK_DOWN:

            fov+=10;
            fov = fov >170 ? 170: fov;
            /* R= rotate(-speed , u);
            lookAt = R*lookAt;*/
            break;



    }
}


void Client::resize(int W, int H) {
    aspect = float(W) / float(H);
    glViewport(0, 0, W, H);
}


void Client::mousedrag(int x, int y, bool left_button) {
    // respond to mouse click
    //   (x,y): click location (in window coordinates)

    if(left_button) {

        Hcoord  u = normalize(cross(lookAt ,EYEAXIS));
        Matrix R1 = rotate(y,u);


        Hcoord  v = normalize(cross(lookAt ,u));
        Matrix R2 = rotate(x,v);

        lookAt = R1*R2*lookAt;

    }


}


/////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {

    // SDL: initialize and create a window
    SDL_Init(SDL_INIT_VIDEO);
    const char *title = "Window Title";
    int width = FRAME_BUFFER_W,
            height = FRAME_BUFFER_H;
    SDL_Window *window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED, width, height,
                                          SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    SDL_GLContext context = SDL_GL_CreateContext(window);

    // GLEW: get function bindings (if possible)
    glewInit();
    if (!GLEW_VERSION_2_0) {
        cout << "needs OpenGL version 2.0 or better" << endl;
        return -1;
    }

    // animation loop
    bool done = false;
    Client *client = new Client();
    Uint32 ticks_last = SDL_GetTicks();
    while (!done) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    done = true;
                    break;
                case SDL_KEYDOWN:
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                        done = true;
                    else
                        client->keypress(event.key.keysym.sym);
                    break;
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                        client->resize(event.window.data1, event.window.data2);
                    break;
                case SDL_MOUSEMOTION:
                    if ((event.motion.state & SDL_BUTTON_LMASK) != 0
                        || (event.motion.state & SDL_BUTTON_RMASK) != 0)
                        client->mousedrag(event.motion.xrel, event.motion.yrel,
                                          (event.motion.state & SDL_BUTTON_LMASK) != 0);
                    break;
            }
        }
        Uint32 ticks = SDL_GetTicks();
        double dt = 0.001 * (ticks - ticks_last);
        ticks_last = ticks;
        client->draw(dt);
        SDL_GL_SwapWindow(window);
    }

    // clean up
    delete client;
    SDL_GL_DeleteContext(context);
    SDL_Quit();
    return 0;
}

