#version 130
out vec3 TexCoords;

uniform vec3 aPos;
uniform mat4 projection;
uniform mat4 view;

void main()
{
    TexCoords = vec3 (1,1,1);
     vec4 pos  = projection * view * vec4(aPos, 1.0);
     gl_Position = pos.xyww;
}