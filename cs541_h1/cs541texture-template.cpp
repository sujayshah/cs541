// cs541texture-template.cpp
// -- template for assignment #4 (textures)
// cs541 10/17
//
// To compile from Visual Studio 2015 command prompt:
//    cl /EHsc cs541texture-template.cpp Affine3D.cpp Plane.cpp opengl32.lib glew32.lib sdl2.lib sdl2main.lib /link /subsystem:console
// To compile from Linux command line:
//    g++ cs541texture-template.cpp Affine3D.cpp Plane.cpp
//        -lGL -lGLEW -lSDL2

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <sstream>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include "Affine3D.h"
#include "Plane.h"
#include "Torus.h"
#include "Sphere.h"
using namespace std;


const int MESH_SIZE = 20;
const float PI = 4.0f*atan(1.0f);
//cam mov variables
Hcoord O(0,0,0,0), EYAXIS(0,1,0,0),look,eye,u, light_position;
int fov=80;
static double dt;

// load a text file into a string
string loadTextFile(const char *fname) {
  string out,
         line;
  ifstream in(fname);
  getline(in,line);
  while (in) {
    out += line + "\n";
    getline(in,line);
  }
  return out;
}


// read a 24-bit color bitmap image file
//   Note: caller is responsible for deallocating the
//         RGB data
unsigned char* loadBitmapFile(const char *fname, int *width, int *height) {
  fstream in(fname,ios_base::in|ios_base::binary);
  char header[38];
  in.read(header,38);
  unsigned offset = *reinterpret_cast<unsigned*>(header+10);
  int W = *reinterpret_cast<int*>(header+18),
      H = *reinterpret_cast<int*>(header+22),
      S = 3*W;
  S += (4-S%4)%4;
  int size = S*H;
  in.seekg(offset,ios_base::beg);
  unsigned char *data = new unsigned char[size];
  in.read(reinterpret_cast<char*>(data),size);
  if (!in) {
    delete[] data;
    return 0;
  }

  for (int j=0; j < H; ++j)
   {
    for (int i=0; i < W; ++i)
     {
      int index = S*j + 3*i;
      swap(data[index+0],data[index+2]);
    }
  }
  *width = W;
  *height = H;
  return data;
}


/////////////////////////////////////////////////////////////////
class Client {
  public:
    Client(void);
    ~Client(void);
    void draw(double dt);
    void keypress(SDL_Keycode kc);
    void resize(int W, int H);
    void mousedrag(int x, int y, bool lbutton);
    void initialise_Sphere();
  private:
    GLuint vertex_buffer,
           normal_buffer,
           face_buffer,
           texcoord_buffer,
           texture_buffer,
           vertex_buffer_sphere,
           normal_buffer_sphere,
           face_buffer_sphere;
    GLint program,
          aposition,
          anormal,
          atexture_coord,
          upersp_matrix,
          uview_matrix,
          umodel_matrix,
          unormal_matrix,
          ulight_position,
          ulight_color;
    int face_count,face_count_sphere;
    float time;
    float aspect;
};


Client::Client(void) {
  // (I) create shader program
  GLint value;

  // (I.A) compile fragment shader
  string fragtextstr = loadTextFile("diffuse_texture.frag");
  const char *fragtext = fragtextstr.c_str();
  GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fshader,1,&fragtext,0);
  glCompileShader(fshader);
  glGetShaderiv(fshader,GL_COMPILE_STATUS,&value);
  if (!value) {
    cerr << "fragment shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(fshader,1024,0,buffer);
    cerr << buffer << endl;
  }

  // (I.B) compile vertex shader
  string verttextstr = loadTextFile("diffuse_texture.vert").c_str();
  const char *verttext = verttextstr.c_str();
  GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vshader,1,&verttext,0);
  glCompileShader(vshader);
  glGetShaderiv(vshader,GL_COMPILE_STATUS,&value);
  if (!value) {
    cerr << "vertex shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(vshader,1024,0,buffer);
    cerr << buffer << endl;
  }

  // (I.C) link shaders
  program = glCreateProgram();
  glAttachShader(program,fshader);
  glAttachShader(program,vshader);
  glLinkProgram(program);
  glGetProgramiv(program,GL_LINK_STATUS,&value);
  if (!value) {
    cerr << "vertex shader failed to compile" << endl;
    char buffer[1024];
    glGetShaderInfoLog(vshader,1024,0,buffer);
    cerr << buffer << endl;
  }
  glDeleteShader(fshader);
  glDeleteShader(vshader);

  // (II) get shader variable locations
  aposition = glGetAttribLocation(program,"position");
  anormal = glGetAttribLocation(program,"normal");
  atexture_coord = glGetAttribLocation(program,"texture_coord");
  upersp_matrix = glGetUniformLocation(program,"persp_matrix");
  uview_matrix = glGetUniformLocation(program,"view_matrix");
  umodel_matrix = glGetUniformLocation(program,"model_matrix");
  unormal_matrix = glGetUniformLocation(program,"normal_matrix");
  ulight_position = glGetUniformLocation(program,"light_position");
  ulight_color = glGetUniformLocation(program,"light_color");

  // (III) upload plane mesh to GPU
  Plane plane(MESH_SIZE);
  face_count = plane.faceCount();

  // (III.A) vertex buffer
  glGenBuffers(1,&vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer);
  int vertex_buffer_size = sizeof(Hcoord)*plane.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,plane.vertexArray(),GL_STATIC_DRAW);

  // (III.B) normal buffer
  glGenBuffers(1,&normal_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,plane.normalArray(),GL_STATIC_DRAW);

  // (III.C) face buffer
  glGenBuffers(1,&face_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,plane.faceArray(),GL_STATIC_DRAW);

  // (III.D) texture coordinate buffer
  glGenBuffers(1,&texcoord_buffer);
  glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);
  float *texcoord = new float[2*plane.vertexCount()];
  Matrix Std2Unit = scale(0.5f,0.5f,1)
                    * translate(Hcoord(1,1,0,0));
  for (int i=0; i < plane.vertexCount(); ++i)
   {
    Hcoord uv = Std2Unit * plane.vertexArray()[i];
    texcoord[2*i+0] = uv[0];
    texcoord[2*i+1] = uv[1];
  }
  glBufferData(GL_ARRAY_BUFFER,2*sizeof(float)*plane.vertexCount(),texcoord,GL_STATIC_DRAW);
  delete[] texcoord;

  // (IV) upload texture to GPU
  int width, height;
  unsigned char *rgbdata = loadBitmapFile("image.bmp",&width,&height);
  glGenTextures(1,&texture_buffer);
  glBindTexture(GL_TEXTURE_2D,texture_buffer);
  glPixelStorei(GL_UNPACK_ALIGNMENT,4);
  glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,rgbdata);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

  //initialise sphere
  initialise_Sphere();
  // (V) enable use of z-buffer
  glEnable(GL_DEPTH_TEST);
 
  //camera look init
  eye=Hcoord (0,0,0,1);
  look= Hcoord(0,0,-1,0);

  //light pos init
  light_position.z=-5;
  
  //set light color

  aspect = 1;
  time = 0;
}

/*void Client:: initialise_Sphere()
{
	//  upload sphere mesh to GPU
  Sphere sphere(MESH_SIZE);
  face_count_sphere = sphere.faceCount();

  //  vertex buffer of sphere
  glGenBuffers(1,&vertex_buffer_sphere);
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer_sphere);
  int vertex_buffer_size = sizeof(Hcoord)*sphere.vertexCount();
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,sphere.vertexArray(),GL_STATIC_DRAW);

  //  normal buffer of sphere
  glGenBuffers(1,&normal_buffer_sphere);
  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer_sphere);
  glBufferData(GL_ARRAY_BUFFER,vertex_buffer_size,sphere.normalArray(),GL_STATIC_DRAW);

  //  face buffer of sphere
  glGenBuffers(1,&face_buffer_sphere);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer_sphere);
  int face_buffer_size = sizeof(Mesh3D::Face)*face_count_sphere;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,face_buffer_size,sphere.faceArray(),GL_STATIC_DRAW);

  // (IV) enable use of z-buffer
  glEnable(GL_DEPTH_TEST);
}*/

Client::~Client(void) {
  // delete the shader program
  glUseProgram(0);
  glDeleteProgram(program);

  // delete the vertex, normal, texture coordinate, and face buffers
  glDeleteBuffers(1,&face_buffer);
  glDeleteBuffers(1,&normal_buffer);
  glDeleteBuffers(1,&vertex_buffer);
  glDeleteBuffers(1,&texcoord_buffer);

  // delete texture buffer
  glDeleteTextures(1,&texture_buffer);
}


void Client::draw(double dt) {
  // clear frame buffer and z-buffer
  glClearColor(1.0f,1.0f,1.0f,1);
  glClear(GL_COLOR_BUFFER_BIT);
  glClearDepth(1);
  glClear(GL_DEPTH_BUFFER_BIT);

  // set shader uniform variable values
  glUseProgram(program);
  glUniform4f(ulight_position, light_position.x, light_position.y, light_position.z,1);
  Matrix P = perspective(fov,aspect,0.1f);
  glUniformMatrix4fv(upersp_matrix,1,true,(float*)&P);
  Matrix V = scale(1);
  //camera movemment computation
  V.rows[0]=normalize(cross(look,EYAXIS));
  V.rows[2]=-normalize(look);
  V.rows[1]=cross(V.rows[2],V.rows[0]);
  V=V*translate(O-eye);
  glUniformMatrix4fv(uview_matrix,1,true,(float*)&V);

  const float RATE = 360.0f/5.0f;
  const Hcoord AXIS(0,1,0,0),
               CENTER(0,0,-3,0);
  Matrix M = translate(CENTER)
             * rotate(RATE*time,AXIS)
             * scale(1.5f);
  glUniformMatrix4fv(umodel_matrix,1,true,(float*)&M);
  glUniformMatrix4fv(unormal_matrix,1,true,(float*)&M);

  const Hcoord LIGHT_POSITION(1,0.5f,0,1),
               LIGHT_COLOR(1,1,1,1);
  glUniform4fv(ulight_position,1,(float*)&LIGHT_POSITION);
  glUniform3fv(ulight_color,1,(float*)&LIGHT_COLOR);

  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER,vertex_buffer);
  glVertexAttribPointer(aposition,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(aposition);

  glBindBuffer(GL_ARRAY_BUFFER,normal_buffer);
  glVertexAttribPointer(anormal,4,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(anormal);

  glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);
  glVertexAttribPointer(atexture_coord,2,GL_FLOAT,false,0,0);
  glEnableVertexAttribArray(atexture_coord);

  // select the texture to use
  glBindTexture(GL_TEXTURE_2D,texture_buffer);

  // draw the mesh
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,face_buffer);
  glDrawElements(GL_TRIANGLES,3*face_count,GL_UNSIGNED_INT,0);

  //camera update pos
 // glUniform4f(ucamera_pos, eye.x, eye.y, eye.z, 1);

 //draw camera obj
 Matrix R_cam_obj = rotate(0*time, AXIS),
	  M_cam_obj = translate(light_position)
	  * R_cam_obj
	  * scale(0.05f);
  //set light object
  glUniformMatrix4fv(umodel_matrix, 1, true, (float*)&M_cam_obj);
  glUniformMatrix4fv(unormal_matrix, 1, true, (float*)&R_cam_obj);
  glUniform3f(ulight_color, 0.0f, 0.0f, 0.0f);
  // set shader attributes
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_sphere);
  glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 0, 0);
  glEnableVertexAttribArray(aposition);
  glBindBuffer(GL_ARRAY_BUFFER, normal_buffer_sphere);
  glVertexAttribPointer(anormal, 4, GL_FLOAT, false, 0, 0);
  glEnableVertexAttribArray(anormal);

  // draw the mesh sphere
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, face_buffer_sphere);
  glDrawElements(GL_TRIANGLES, 3 * face_count_sphere, GL_UNSIGNED_INT, 0);

  time += dt;
}


void Client::keypress(SDL_Keycode kc) 
{
  // respond to keyboard input
  //   kc: SDL keycode (e.g., SDLK_SPACE, SDLK_a, SDLK_s)
    const float XLAT=0.1f,source_speed=5.0f;
    switch(kc)
    {
        case SDLK_RIGHT:
          u=normalize(cross(look,EYAXIS));
          eye=eye+XLAT*u;
          break;
          
        case SDLK_LEFT:
          u=normalize(cross(look,EYAXIS));
            eye=eye-XLAT*u;
          break;
        
        case SDLK_UP:
          eye=eye+XLAT*look;
          break;
          
        case SDLK_DOWN:
          eye=eye-XLAT*look;
          break;
          
        case SDLK_w:
          if(fov>120)
            break;
          else
          fov+=20;
          break;
          
        case SDLK_s:
          if(fov<30)
            break;
          else;
          fov-=20;
          break;

        //light movement
      case SDLK_KP_5: //forward z
        light_position.z -= source_speed*dt;
        break;
      case SDLK_KP_2:// backward z
        light_position.z += source_speed*dt;
        break;
      case SDLK_KP_1://left x
        light_position.x -= source_speed*dt;
        break;
      case SDLK_KP_3://right x
        light_position.x += source_speed*dt;
        break;
      case SDLK_KP_7://up y
        light_position.y += source_speed*dt;
        break;
      case SDLK_KP_4://down y
        light_position.y -= source_speed*dt;
        break;
     }
}

void Client::resize(int W, int H) 
{
  aspect = float(W)/float(H);
  glViewport(0,0,W,H);
}


void Client::mousedrag(int x, int y, bool left_button) 
{
  // respond to mouse click
  //   (x,y): click location (in window coordinates)
    Matrix r;
  if(left_button)
  {
      const float ROT_INC=0.5;
      u=normalize(cross(look,EYAXIS));
      Matrix R=rotate(y*ROT_INC,u);
      look=R*look;
      
      u=normalize(cross(look,u));
      r=rotate(x*ROT_INC,u);
      look=r*look;
	}
}


/////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {

  // SDL: initialize and create a window
  SDL_Init(SDL_INIT_VIDEO);
  const char *title = "Window Title";
  int width = 500,
      height = 500;
  SDL_Window *window = SDL_CreateWindow(title,SDL_WINDOWPOS_UNDEFINED,
                                        SDL_WINDOWPOS_UNDEFINED,width,height,
                                        SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
  SDL_GLContext context = SDL_GL_CreateContext(window);

  // GLEW: get function bindings (if possible)
  glewInit();
  if (!GLEW_VERSION_2_0) {
    cout << "needs OpenGL version 2.0 or better" << endl;
    return -1;
  }

  // animation loop
  bool done = false;
  Client *client = new Client();
  Uint32 ticks_last = SDL_GetTicks();
  while (!done) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          done = true;
          break;
        case SDL_KEYDOWN:
          if (event.key.keysym.sym == SDLK_ESCAPE)
            done = true;
          else
            client->keypress(event.key.keysym.sym);
          break;
        case SDL_WINDOWEVENT:
          if (event.window.event == SDL_WINDOWEVENT_RESIZED)
            client->resize(event.window.data1,event.window.data2);
          break;
        case SDL_MOUSEMOTION:
          if ((event.motion.state&SDL_BUTTON_LMASK) != 0
              || (event.motion.state&SDL_BUTTON_RMASK) != 0)
            client->mousedrag(event.motion.xrel,event.motion.yrel,
                              (event.motion.state&SDL_BUTTON_LMASK) != 0);
          break;
      }
    }
    Uint32 ticks = SDL_GetTicks();
    double dt = 0.001*(ticks - ticks_last);
    ticks_last = ticks;
    client->draw(dt);
    SDL_GL_SwapWindow(window);
  }

  // clean up
  delete client;
  SDL_GL_DeleteContext(context);
  SDL_Quit();
  return 0;
}

