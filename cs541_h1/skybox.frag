#version 130

out vec4 fragcolor;
uniform vec3 ambient_color;

in vec4 normal_vector;
in vec4 light_vector;
in vec2 vtexture_coord;
in vec4 view_vector;


uniform sampler2D skybox;

void main()
{
    vec3 res  = texture(skybox, vtexture_coord).xyz;
 //   vec3 light  = res*ambient_color;
    fragcolor = vec4(res , 1);
}
