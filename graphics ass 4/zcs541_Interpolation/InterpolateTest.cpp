/*
CS-541
Student ID:60001017
Student name: Udippon das
Professor J hanson
*/



#include "Interpolate.h"
#include "time.h"

void GenerateCoord(Hcoord &pt)
 {
	pt.x  = rand() % 900 + 0; 
	pt.y  = rand() % 900 + 0; 
	pt.z  = rand() % 900 + 0;
	pt.w = 1.0f;
	pt.z*=-1; 
}

int main() 
{

	srand(time(NULL));
	Hcoord Pcam,Qcam,Rcam;
	GenerateCoord(Pcam);
	GenerateCoord(Qcam);
	GenerateCoord(Rcam);  

	float testLamda , testMu , testTau;
	float sum = 0;
	int i = 0;
	float scalars[3];

	for(i=0;i<3;i++)
	{
		scalars[i] = rand() % 100;
		sum += scalars[i];	
	}

	testLamda = scalars[0] / sum;
	testMu 	  = scalars[1] / sum;
	testTau   = scalars[2] / sum; 

	std::cout<<"Scalars :"<<testLamda<<" , "<<testMu<<" , "<<testTau<<std::endl;
	std::cout<<std::endl<<"---------------------------------------------------------------------------"<<std::endl;

	std::cout<<"Pcam ("<<Pcam.x<<" , "<<Pcam.y<<" , "<<Pcam.z<<")"<<std::endl;
	std::cout<<"Qcam ("<<Qcam.x<<" , "<<Qcam.y<<" , "<<Qcam.z<<")"<<std::endl;
	std::cout<<"Rcam ("<<Rcam.x<<" , "<<Rcam.y<<" , "<<Rcam.z<<")"<<std::endl;

	Matrix pMat = perspective(80.0f,1.0,.1f);
	Hcoord Icam = (testLamda*Pcam) + (testMu*Qcam) + (testTau*Rcam);

	std::cout<<"Icam ("<<Icam.x<<" , "<<Icam.y<<" , "<<Icam.z<<" , "<<Icam.w<<")"<<std::endl;
	Hcoord I = pMat*Icam;

	std::cout<<"I ("<<I.x<<" , "<<I.y<<" , "<<I.z<<" , "<<I.w<<")"<<std::endl;
	std::cout<<std::endl<<"---------------------------------------------------------------------------"<<std::endl;

	Hcoord result(I.x/I.w , I.y/I.w , I.z/I.w , 1); 
	Interpolate interpolate(pMat*Pcam, pMat*Qcam ,pMat*Rcam );

	float z = interpolate.setPixel(result.x , result.y);

	std::cout<<"Original z : "<<result.z<<std::endl;
	std::cout<<"Inerpolated z : "<<z<<std::endl;

	float l1 = interpolate(1,0,0);
	std::cout<<"Original l : "<<testLamda<<std::endl;
	std::cout<<"calculated l  : "<<l1<<std::endl;

	float m1 = interpolate(0,1,0);
	std::cout<<"Original m : "<<testMu<<std::endl;
	std::cout<<"calculated m  : "<<m1<<std::endl;

	float d1 = interpolate(0,0,1);
	std::cout<<"Original d : "<<testTau<<std::endl;
	std::cout<<"calculated d  : "<<d1<<std::endl;

	return 0;
}