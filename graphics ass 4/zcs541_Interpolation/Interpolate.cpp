/*
CS-541
Student ID:60001017
Student name: Udippon das
Professor J hanson
*/

#include "Interpolate.h"

Interpolate::Interpolate(const Hcoord& P1 , const Hcoord& Q1 , const Hcoord& R1 ) 
{
	this->P =  P1;
	this->P.x = this->P.x/this->P.w;
	this->P.y = this->P.y/this->P.w;
	this->P.z = this->P.z/this->P.w;
	
	this->Q =  Q1;
	this->Q.x = this->Q.x/this->Q.w;
	this->Q.y = this->Q.y/this->Q.w;
	this->Q.z = this->Q.z/this->Q.w;
	
	this->R =  R1;
	this->R.x = this->R.x/this->R.w;
	this->R.y = this->R.y/this->R.w;
	this->R.z = this->R.z/this->R.w;
}

float Interpolate::setPixel(float x ,  float y)
 {
	//linear Interpolation by geometry method
	Hcoord Origin(0,0,0,0);
	Hcoord normal = cross((Q-P),(R-P));
	float d = dot(normal,(P-Origin));

	float z = (d-(normal.x*x)-(normal.y*y)) / normal.z;

	Hcoord I(x,y,z,1);

	lambda = abs(cross((Q-I) , (R -I))) /abs(cross((Q - P),(R - P))) ;
	mu = abs(cross((R-I) , (P -I))) /abs(cross((Q - P),(R - P))) ;
	tau = abs(cross((P-I) , (Q-I))) /abs(cross((Q - P),(R - P))) ;

	return z;
}

float  Interpolate::operator()(float vP , float vQ , float vR) 
{
	float w_depth = lambda/P.w + mu/Q.w + tau/R.w;
	return ((((lambda/P.w)/w_depth)*vP) + (((mu/Q.w)/w_depth)*vQ) + (((tau/R.w)/w_depth)*vR));
}