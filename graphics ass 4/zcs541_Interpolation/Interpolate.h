/*
CS-541
Student ID:60001017
Student name: Udippon das
Professor J hanson
*/


#ifndef  INTERPOLATE_H
#define  INTERPOLATE_H
#include "Affine3D.h"
#include <iostream>

class Interpolate 
{
	public:
	Interpolate(const Hcoord &P ,const Hcoord& Q , const Hcoord& R ) ;
	float setPixel(float x , float y);
	float operator()(float vP , float vQ , float vR);

	private:
	Hcoord P,Q,R;
	float lambda,mu,tau;

};
#endif