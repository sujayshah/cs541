#include "Interpolate.h"
Interpolate::Interpolate(const Hcoord& P1 , const Hcoord& Q1 , const Hcoord& R1 ) 
{

	 this->P=P1;
    this->Q=Q1;
    this->R=R1;

	std::cout<<"P Interpolate "<<"p1="P.x" "<<" , "<<"p2="P.y" "<<" , "<<"p3"=P.z" "<<std::endl;
	std::cout<<"Q Interpolate "<<"r1="R.x" "<<" , "<<"r2="R.y" "<<" , "<<"r3="R.z" "<<std::endl;
	std::cout<<"R Interpolate "<<"q1="Q.x" "<<" , "<<"q2="Q.y" "<<" , "<<"q3="Q.z" "<<std::endl;
}


float Interpolate::setPixel(float x ,  float y) {

	/*Linear Interpolation*/

	Hcoord Origin(0,0,0,0);
	Hcoord m = cross((Q-P) , (R-P));
	float d = dot(m,(P-Origin));

	float z = (d-(m.x*x)-(m.y*y)) / m.z;

	Hcoord I(x,y,z,1);

	lambda = abs(cross((Q-I) , (R -I))) /abs(cross((Q - P),(R - P))) ;
	mu = abs(cross((R-I) , (P -I))) /abs(cross((Q - P),(R - P))) ;
	delta = abs(cross((P-I) , (Q-I))) /abs(cross((Q - P),(R - P))) ;

	return z;
}

float  Interpolate::operator()(float vP , float vQ , float vR) {

	float depth = lambda/P.w + mu/Q.w + delta/R.w;
	
	return ((((lambda/P.w)/depth)*vP) + (((mu/Q.w)/depth)*vQ) + (((delta/R.w)/depth)*vR));
}