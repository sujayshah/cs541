#include "Interpolate.h"
#include "time.h"


#define SCREEN_WIDTH  800.0f
#define SCREEN_HEIGHT 800.0f


void GenerateRandomPoint(Hcoord &point) {


	point.x  = rand() % 800 + 0; 
	point.y  = rand() % 800 + 0; 
	point.z  = rand() % 800	 + 0;
	point.w = 1.0f;
	point.z*=-1; 
}

void GenerateScalar(float &lambda ,float &mu , float &delta ) {

	int sum = 0;
	int i = 4;
	int nums[3];

	while(--i) {

		nums[3-i] = rand() % 100;
		sum += nums[3-i];
		
	}

	lambda 	= (float) nums[0] / sum;
	mu 		= (float) nums[1] / sum;
	delta 	= (float) nums[2] / sum; 


}

int main() {

	srand(time(NULL));

	Hcoord Pcamera,Qcamera,Rcamera;

	int simulatecount = 10;
	int simulation_no_ = 0;
	while(--simulatecount) {

		++simulation_no_;

		std::cout<<"______________________________Starting Simulation "<<simulation_no_<<" ________________________________";
		std::cout<<std::endl;		
		std::cout<<std::endl;

		GenerateRandomPoint(Pcamera);
		GenerateRandomPoint(Qcamera);
		GenerateRandomPoint(Rcamera);  

		float lambda , mu , delta;
		GenerateScalar(lambda , mu , delta);
		
		std::cout<<"Scalars :"<<lambda<<" , "<<mu<<" , "<<delta<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;



		std::cout<<"Pcamera "<<Pcamera.x<<" , "<<Pcamera.y<<" , "<<Pcamera.z<<std::endl;
		std::cout<<"Qcamera "<<Qcamera.x<<" , "<<Qcamera.y<<" , "<<Qcamera.z<<std::endl;
		std::cout<<"Rcamera "<<Rcamera.x<<" , "<<Rcamera.y<<" , "<<Rcamera.z<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;


		Matrix projection_matrix = perspective(80.0f,1.0,.1f);
		Hcoord Icamera = (lambda*Pcamera) + (mu*Qcamera) + (delta*Rcamera);

		std::cout<<"Icamera x : "<<Icamera.x<<std::endl;
		std::cout<<"Icamera y : "<<Icamera.y<<std::endl;
		std::cout<<"Icamera z : "<<Icamera.z<<std::endl;
		std::cout<<"Icamera w: "<<Icamera.w<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;



		Hcoord I =Icamera;


		std::cout<<"I x : "<<I.x<<std::endl;
		std::cout<<"I y : "<<I.y<<std::endl;
		std::cout<<"I z : "<<I.z<<std::endl;
		std::cout<<"I w: "<<I.w<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;


		Hcoord result(I.x/I.w , I.y/I.w , I.z/I.w , 1); 


		Interpolate ip(Pcamera, Qcamera ,Rcamera );
		std::cout<<std::endl;
		std::cout<<std::endl;
/*-		std::cout<<"Original x : "<<result.x<<std::endl;
		std::cout<<"Original y : "<<result.y<<std::endl;
		std::cout<<"Original z : "<<result.z<<std::endl;
*/
		float z = ip.setPixel(result.x , result.y);

		std::cout<<"Original z : "<<result.z<<std::endl;
		std::cout<<"Inerpolated z : "<<z<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;

		float l = ip(1,0,0);
		std::cout<<"Original lambda : "<<lambda<<std::endl;
		std::cout<<"calculated lambda  : "<<l<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;

		float m = ip(0,1,0);
		std::cout<<"Original mu : "<<mu<<std::endl;
		std::cout<<"calculated mu  : "<<m<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;

		float d = ip(0,0,1);
		std::cout<<"Original delta : "<<delta<<std::endl;
		std::cout<<"calculated delta  : "<<d<<std::endl;
		std::cout<<std::endl;
		std::cout<<std::endl;

		std::cout<<"______________________________End of Simulation "<<simulation_no_<<" ________________________________";
		std::cout<<std::endl;		
		std::cout<<std::endl;

	}

	return 0;
}