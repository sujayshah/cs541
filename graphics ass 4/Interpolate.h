#ifndef  _INTERPOLATE_H_
#define  _INTERPOLATE_H_

#include "Affine3D.h"
#include <iostream>

class Interpolate {
	
	public:
	
	Interpolate(const Hcoord &P ,const Hcoord& Q , const Hcoord& R ) ;

	float setPixel(float x , float y);

	float operator()(float vP , float vQ , float vR);

	private:

	Hcoord P;
	Hcoord Q;
	Hcoord R;

	float lambda;
	float mu;
	float delta;
};


#endif